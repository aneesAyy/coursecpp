#include <iostream>
#include <vector>
using namespace std;

int main(int argc, char const *argv[])
{
    // int num {200};
    // int *numPtr {&num};
    // int &ref{num}; // reference to number, an alias

    // cout << "The num is " << num << endl;
    // cout << "The address is " << numPtr << endl;
    // cout << "The number using alias is" << ref << endl << endl;

// using ranged based for loops

    // vector <string> names {"Meh", "felin", "betyr", "igg"};
    string names[] {"meh", "sad", "idk"};

    // for (auto &i : names) // if "&" is removed no array value wont change
    // {
    //     i = "phunny";
    // }
    // for (auto i : names)
    // {
    //     cout << i << " ";
    // }
    
// referencing with pointers 
    
    string *namesPtr{nullptr};
    namesPtr = names; // apparently this syntax only works with arrays not vectors

    for (auto &&i : names)
    {
        cout << *(namesPtr++) << endl;
    }
    


    return 0;
}
