#include <iostream>
using namespace std;

int main(int argc, char const *argv[])
{
    int scores[] {10, 23, 90, 69, 70};
    int *scorePtr {scores};

    while (*scorePtr != 70)
    {
        cout << *scorePtr << endl;
        scorePtr++;
    }
    

    return 0;
}
