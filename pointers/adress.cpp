#include <iostream>
using namespace std;

int main(int argc, char const *argv[])
{
/*
    int *p;
    cout << "The value of p is: " << p << endl;
    cout << "The address of p is " << &p << endl;
    cout << "The size of p is " << sizeof p << endl;
    p = nullptr;
    cout << "The value of p now is :" << p << endl;
    return 0;

*/
    // More on pointers

    int *x; 
    int score{10};
    
    cout << "\nSize of the int is " << sizeof score << " bytes"<< endl;
    cout << "\nAddress of score is: "<<  &score << endl;
    // cout << &x << endl;

    x = &score; 
    cout << "\nThe value stored in the pointer is: "<< x << endl;
    cout << "The address of the pointer is: " << &x << endl;
    return 0;
}
