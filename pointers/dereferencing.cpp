#include <iostream>
#include <vector>
using namespace std;

// dereferencing is used to access the value a pointer, is pointing to...something simple like an int or complex as a vector

int main(int argc, char const *argv[])
{
/*
    int score {100};
    int *scorePntr {&score};

    cout << *scorePntr << endl << endl;

    *scorePntr = 500;

    cout << score << endl;
    cout << *scorePntr << endl;
*/
    // Vectors 
    vector <string> names {"Me", "wants", "tuu", "diee"};
    vector <string> *namesPtr {nullptr};

    namesPtr = &names;

    cout << "\nThe first name is: " << (*namesPtr).at(0) << endl;

    for (auto &&i : *namesPtr)
    {
        cout << i << endl;        
    }
    
    

}
