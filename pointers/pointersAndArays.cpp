#include <iostream>
using namespace std;

int main(int argc, char const *argv[])
{
    int scores[] {20, 40, 70, 120};

    int *scorePtr{scores};

    // Both display the same adress 

    cout << scores << endl; // first array value address
    cout << scorePtr << endl; // value the pointer is pointing to, same as above

    cout << "\nHow it works!" << endl;
    
    cout << scorePtr + 1 << endl;   // previous value + 4 (Int size)
    cout << *(scorePtr + 1) << endl;    // dereferencing
    cout << scorePtr[1] << endl;    // same result
    cout << scores[1] << endl;      // same result

    cout << endl << scorePtr + 2 << endl;   // previous value + 4
    cout << *(scorePtr + 2) << endl;    // dereferencing
    
    return 0;
}
