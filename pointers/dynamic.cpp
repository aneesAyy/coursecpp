#include <iostream>
#include <vector>

using namespace std;

int main(int argc, char const *argv[])
{
    // Dynamic allocations for int in pointers 

    int *intPtr {nullptr};

    intPtr = new int;
    cout << intPtr << endl;
    delete intPtr;

    // Same thing for a doubles array

    size_t size {};
    double *doublePtr {nullptr};

    cout << "How many doubles?" << endl;
    cin >> size;

    doublePtr = new double[size];

    cout << doublePtr; // Displays address of the first pointer in the array

    delete [] doublePtr; // "[]" because its an array, stupad

    return 0;
}
