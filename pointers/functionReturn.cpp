#include <iostream>
using namespace std;

int *largestNumber(int *num1, int *num2); 
int *createArray(size_t size, int initial);
void display(int anyArray, size_t size);

int main(int argc, char const *argv[])
{
    // int a {10};
    // int b {200};
    // int *largestPnt = largestNumber(&a, &b);
    // cout << largestPnt << endl;
   
    int size{};        
    cout << "How many integers would you like to allocate!" << endl;
    cin >> size;
    int initial {};
    cout << "What value would they be initialized to" << endl;
    cin >> initial;

    // Create a temporary array to access the values from the pointer
    int *myArray;
    myArray = createArray(size, initial); 

    cout << "\nThe adress of the first element in array is: " << myArray << endl;
    cout << "The first element of array is: " << *myArray << endl << endl;

    // Display the array

    void display(int myArray, size_t size);
    
    delete [] myArray;
    return 0;
}

// Finds largest number and returns  its address 

int *largestNumber(int *num1, int *num2) {
    if(*num1 > *num2)
        return num1;
    else 
        return num2;
}

int *createArray(size_t size, int initial) {

    int *newStorage {nullptr};

    newStorage = new int[size];

    for (size_t i = 0; i < size; i++)
    {
        *(newStorage + i) = initial;
    }

    return newStorage;

}

void display(int anyArray, size_t size) {
    for (size_t i = 0; i < size; i++)
    {
        cout << (anyArray + i) << " ";
    }
    cout << endl;
}

