#include <iostream>
#include <cctype>
#include <queue>
#include <stack>
#include <string>
#include <iomanip>
#include <algorithm>

// bool isPalindrome(std::string);

bool isPalindrome(std::string &s)
{
    std::stack<char> stk;
    std::queue<char> q;

    for (auto &&c : s)
    {
        if (std::isalpha(c))
        {
            c = std::toupper(c);
            q.push(c);
            stk.push(c);
        }
    }
    char c1{};
    char c2{};

    while (!q.empty())
    {
        c1 = q.front();
        q.pop();
        c2 = stk.top();
        stk.pop();
        if (c1 != c2)
            return false;
    }

    return true;
}

int main(int argc, char const *argv[])
{
    std::vector<std::string> testStrings{"sadidas", "lalalala", "lOloLoL", "aba", "abba", "abbcbba", "nasa at me", "A santa at NASA", "c++"};

    std::cout << std::boolalpha;
    std::cout << std::setw(10) << std::left << "Result"
              << "String" << std::endl;

    for (auto &&s : testStrings)
    {
        std::cout << std::setw(10) << isPalindrome(s) << s << std::endl;
    }

    std::string input{};
    std::cout << "\nInput a string to check" << std::endl;
    std::getline(std::cin, input);

    std::cout << "Palindrome: " << isPalindrome(input) << std::endl;

    return 0;
}
