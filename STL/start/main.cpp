#include <iostream> 
#include <algorithm>
#include <vector>

using namespace std;

int main(int argc, char const *argv[])
{
    vector<int> v{6,1,7,32,6,14};
    sort(v.begin(), v.end());   // Sort function
    int sum {};

    for (auto &&i : v)
    {   
        cout << i << endl;
    }
    
    return 0;
}
