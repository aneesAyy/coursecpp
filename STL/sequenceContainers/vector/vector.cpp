#include <iostream>
#include <algorithm>
#include <vector>

template <typename T>
void display(std::vector<T> &vec)
{
    std::cout << "\n[ ";
    std::for_each(vec.begin(), vec.end(), [](int x) { std::cout << x << " "; });
    std::cout << " ]" << std::endl;
}

int main(int argc, char const *argv[])
{
    std::vector<int> vec1{1, 2, 3, 4, 5, 6, 7, 8, 9};
    std::vector<int> vec2{10, 20, 30, 40, 50, 60};

    display(vec1);

    auto it1 = std::find(vec1.begin(), vec1.end(), 3);
    // vec1.insert(it1, 100);

    // std::copy(vec1.begin(), vec1.end(), std::back_inserter(vec2));

    std::copy_if(
        vec1.begin(), vec1.end(), std::back_inserter(vec2), [](int x) { return x % 2 == 0; });
    // even only copy
    display(vec2);

    std::vector<int> vec3{};

    std::transform(vec1.begin(), vec1.end(), vec2.begin(), std::back_inserter(vec3), [](int x, int y) { return x * y; });
    display(vec3);

    // Insert before 360 in vec3
    display(vec1);

    auto it2 = std::find(vec1.begin(), vec1.end(), 5);
    if (it2 != vec1.end())
    {
        std::cout << "Inserting...";
        vec1.insert(it2, vec3.begin(), vec3.end());
    }
    else
    {
        std::cout << "Sorry, 360 not found" << std::endl;
    }

    display(vec1);

    return 0;
}
