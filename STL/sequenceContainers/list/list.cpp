#include <iostream>
#include <list>
#include <algorithm>
#include <iterator>

class Person
{
    friend std::ostream &operator<<(std::ostream &os, const Person &p);
    std::string name;
    int age;

public:
    Person() : name{"Unknown"}, age{0} {}
    Person(std::string name, int age) : name{name}, age{age} {}
    bool operator<(const Person &rhs) const
    {
        return this->age < rhs.age;
    }
    bool operator==(const Person &rhs) const
    {
        return (this->name == rhs.name && this->age == rhs.age);
    }
};

template <typename T>
void display(std::list<T> &l)
{
    std::cout << "[ ";
    for (auto &&values : l)
    {
        std::cout << values << " ";
    }

    std::cout << "]" << std::endl;
}

std::ostream &operator<<(std::ostream &os, const Person &p)
{
    os << '[' << p.name << ": " << p.age << ']';
    return os;
}

int main(int argc, char const *argv[])
{

    std::list<Person> players;
    players.emplace_back("Ooga", 65);
    players.emplace_back("Blooga", 24);
    players.emplace_back("skoodle", 90);

    display(players);
    players.resize(4);
    display(players);

    // std::list<int> l1{1, 2, 3, 5, 6, 7, 8, 10};
    // auto it = std::find(l1.begin(), l1.end(), 6);

    // std::list<int> l2{100, 200, 300, 500, 600, 700, 800};

    // l1.insert(it, l2.begin(), l2.end());

    // display(l1);
    auto itPlayer = std::find(players.begin(), players.end(), Person{"Ooga", 18});
    if (itPlayer != players.end())
    {
        std::cout << "Ooga found" << std::endl;
    }

    // add new players
    std::string name;
    int age;
    std::cout << "Enter new player name" << std::endl;
    std::getline(std::cin, name);
    std::cout << "Enter new player age" << std::endl;
    std::cin >> age;

    players.emplace_back(name, age);
    display(players);

    // sorting
    players.sort();
    std::cout << "After sorting..." << std::endl;
    display(players);

    return 0;
}
