#include <iostream>
#include <list>
#include <string>
#include <iterator>
#include <iomanip>
#include <limits>

class Song
{
    friend std::ostream &operator<<(std::ostream &os, const Song &p);
    std::string name;
    std::string artist;
    int rating;

public:
    Song() : name{"Unknown"}, artist{"Unknown"}, rating{0} {}
    Song(std::string name, std::string artist, int rating) : name{name}, artist{artist}, rating{rating} {}

    std::string getName() const
    {
        return name;
    }
    std::string getArtist() const
    {
        return artist;
    }
    int getRating() const
    {
        return rating;
    }

    bool operator<(const Song &rhs) const
    {
        return this->rating < rhs.rating;
    }
    bool operator==(const Song &rhs) const
    {
        return (this->name == rhs.name && this->rating == rhs.rating);
    }
};

std::ostream &operator<<(std::ostream &os, const Song &s)
{
    os << std::setw(20) << std::left << s.name
       << std::setw(20) << s.artist
       << std::setw(2) << s.rating;
    return os;
}

void displayCurrent(std::list<Song> &songs, Song &currentSong)
{
    std::cout << std::setw(41) << std::setfill('-') << "" << std::setfill(' ') << std::endl;
    std::cout << "Currently playing:" << std::endl
              << currentSong << std::endl;
}

void displayPlaylist(std::list<Song> &songs)
{
    for (auto &&s : songs)
    {
        std::cout << s << std::endl;
    }
}

void displayMenu()
{
    std::cout << "Welcome to this playlist - Enter an operation or 'Q' to quit " << std::endl;
    std::cout << "F - Play first song" << std::endl;
    std::cout << "N - Play next song" << std::endl;
    std::cout << "P - Play previous song" << std::endl;
    std::cout << "A - Add song at current location" << std::endl;
    std::cout << "L - List current playlist" << std::endl;
}

int main(int argc, char const *argv[])
{
    std::list<Song> songs{
        {"Gods plan", "Drake", 7},
        {"The Box", "Drip4port", 8},
        {"Moonlight", "xxxtentacion", 9},
        {"Barafbari", "someNiga", 6},
        {"someTurkishTrap", "someSingaa", 8}};

    displayMenu();

    char select{};
    auto currentSong = songs.begin();

    do
    {

        switch (toupper(select))
        {
        case 'F':
            currentSong = songs.begin();
            break;
        case 'N':
            currentSong++;
            if (currentSong == songs.end())
                currentSong = songs.begin();
            break;
        case 'P':
            if (currentSong == songs.begin())
                currentSong = songs.end();
            currentSong--;
            break;
        case 'L':
            displayPlaylist(songs);
            break;
        case 'A':
        {
            std::string name;
            std::string artist;
            int rating;
            std::string test;

            std::getline(std::cin, test);

            std::cout << "Enter song name" << std::endl;
            std::getline(std::cin, name);

            std::cout << "Enter artist name" << std::endl;
            std::getline(std::cin, artist);

            std::cout << "Enter song rating" << std::endl;
            std::cin >> rating;

            songs.insert(currentSong, Song{name, artist, rating});
            currentSong--;
        }
        break;
        default:
            break;
        }

        displayCurrent(songs, *currentSong);

        std::cout << std::setw(41) << std::setfill('=') << "" << std::setfill(' ') << std::endl;
        std::cout << "Enter a selection('Q' to quit)" << std::endl;
        std::cin >> select;
    } while (select != 'q' && select != 'Q');

    return 0;
}
