#include <iostream>
#include <stack>

template <typename T>
void display(std::stack<T> s)
{
    std::cout << "[ ";
    while (!s.empty())
    {
        T temp = s.top();
        s.pop();
        std::cout << temp << " ";
    }
    std::cout << "]" << std::endl;
}

int main(int argc, char const *argv[])
{
    std::stack<int> s;

    for (auto &&i : {1, 2, 3, 4, 5})
    {
        s.push(i);
    }

    display(s);

    s.push(100);
    display(s);

    std::cout << s.size() << std::endl;
    s.pop();

    display(s);

    while (!s.empty()) //* Clears the stack
    {
        s.pop();
    }
    display(s);
    return 0;
}
