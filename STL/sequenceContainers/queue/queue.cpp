#include <iostream>
#include <queue>

template <typename T>
void display(std::queue<T> q)
{
    std::cout << "[ ";
    while (!q.empty())
    {
        T temp = q.front();
        std::cout << temp << " ";
        q.pop();
    }
    std::cout << "]" << std::endl;
}

int main(int argc, char const *argv[])
{
    std::queue<int> q;

    for (auto &&i : {1, 2, 3, 4, 5})
    {
        q.push(i);
    }

    display(q);
    std::cout << "Front: " << q.front() << std::endl;
    std::cout << "back: " << q.back() << std::endl;

    q.pop();
    q.pop();
    display(q);

    q.push(100);
    q.push(1000);
    display(q);

    return 0;
}
