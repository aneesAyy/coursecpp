#include <string>
#include <iterator>
#include <iostream>
#include <algorithm>
#include <array>
#include <numeric>

template <typename T>
void display(std::array <T, 5> &arr) {
    std::cout << "\n[ ";
    for (auto &&i : arr)
    {
        std::cout << i << " ";
    }
    std::cout << " ]" << std::endl; 
}

int main()
{
    std::array<int, 5> a1{5,23,6,2,7};  // double-braces required in C++11 prior to the CWG 1270 revision
    std::array<double, 5> a2{5.90,2.9,6.5,2.12,7.1};  // double-braces required in C++11 prior to the CWG 1270 revision
    std::array<std::string, 5> a3 = {std::string("a"), "b"};

    std::array<int, 5> a4;
    // container operations are supported
    std::sort(a1.begin(), a1.end());
    // display(a1);
    // display(a2);
    // display(a3);
    // display(a4);

    int sum = std::accumulate(a1.begin(), a1.end(), 0);
    std::cout << "Sum of the elements is: " << sum << std::endl; 

    return 0;
}