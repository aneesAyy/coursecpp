#include <iostream>

// using namespace std; // Remove std namespace to use template function or use ::max
template <typename T>
T max(T a, T b)
{
    return (a > b) ? a : b;
}

template <typename T1, typename T2>
void func(T1 a, T2 b)
{
    std::cout << a << " " << b << std::endl;
}

template <typename S>
void swap(S &a, S &b)
{
    S temp = a;
    a = b;
    b = temp;
}

struct Person
{
    std::string name;
    int age;
    bool operator>(const Person &rhs) const
    {
        return this->age > rhs.age;
    }
};

std::ostream &operator<<(std::ostream &os, const Person &p)
{
    os << p.name;
    return os;
};

int main(int argc, char const *argv[])
{
    // Simple template
    std::cout << max<int>(5, 12) << std::endl;
    std::cout << max('A', 'b') << std::endl;

    std::cout << "------------------------------" << std::endl;
    // Two args template
    func(123.42, "FuKA");
    func('a', std::string{"henlo there"});

    std::cout << "------------------------------" << std::endl;
    // Class template
    Person p1{"ooga", 20};
    Person p2{"skoodle", 24};

    Person p3 = max(p1, p2);
    std::cout << p3.name << " is older" << std::endl;

    // Overloaded
    std::cout << "------------------------------" << std::endl;
    func(p1, p2);
    std::cout << "------------------------------" << std::endl;
    std::string s1 {"Fk"}, s2 {"You"};
    std::cout << s1 << " " << s2 << std::endl;
    swap(s1, s2);
    std::cout << s1 << " " << s2 << std::endl;
    return 0;
}
