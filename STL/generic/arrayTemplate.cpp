#include <iostream>
#include <string>

template <typename T ,int N>
class Array
{
private:
    int size{N};
    T values[N];

    friend std::ostream &operator<<(std::ostream &os, const Array<T ,N> &arr)
    {
        os << "[ ";
        for (const auto &val : arr.values)
        {
            os << val << " ";
        }
        os << " ]";
        return os;
    }

public:
    Array() = default;
    Array(T initValue)
    {
        for (auto &&item : values)
        {
            item = initValue;
        }
    }

    void fill(T val)
    {
        for (auto &&item : values)
        {
            item = val;
        }
    }

    int getSize()
    {
        return size;
    }

    T &operator[](int index) {
        return values[index];
    }

};

int main(int argc, char const *argv[])
{
    Array<std::string ,5> arr1 (std::string{"bruh"});
    std::cout << arr1 << std::endl;
    std::cout << arr1.getSize() << std::endl;
    arr1.fill("booga");

    arr1[1] = std::string{"ooga"};
    arr1[3] = std::string{"ooga"};

    std::cout << arr1 << std::endl;

    Array<int, 100> arr2 {1};
    std::cout << arr2 << std::endl;
    return 0;
}
