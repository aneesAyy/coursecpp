#include <iostream>
#include <string>
#include <vector>

template <typename T>
class Item
{
private:
    std::string num;
    T value;

public:
    // std::string name;

    Item(std::string num, T value) : num{num}, value{value} {}
    std::string getNum() { return num; }
    T getValue() { return value; }
};

template <typename T1, typename T2>
struct MyPair
{
    T1 first;
    T2 second;
};

int main(int argc, char const *argv[])
{
    Item<std::string> item1{"khraoos", "professor"};
    Item<int> item2{"Niga", 90912};

    std::cout << item1.getNum() << " " << item1.getValue() << std::endl;
    std::cout << item2.getNum() << " " << item2.getValue() << std::endl;

    Item<Item<std::string>> item3{"notAsian", {"chineese", "C++"}};
    std::cout << item3.getNum() << "\t" << item3.getValue().getNum() << " " << item3.getValue().getValue() << std::endl;

    std::cout << "----------------------" << std::endl;
    MyPair<double, int> pair1{13.45, 1200};
    std::cout << pair1.first << " " << pair1.second << std::endl;

    std::cout << "======================" << std::endl;
    std::vector<Item<double>> vec{};
    vec.push_back(Item<double>{"Larry", 100.8});
    vec.push_back(Item<double>{"Ooga", 169.9});
    vec.push_back(Item<double>{"Larry", 902.5});

    for (auto &&i : vec)
    {
        std::cout << i.getNum() << " " << i.getValue() << std::endl;
    }

    return 0;
}
