#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <cctype>

class Person
{
    std::string name;
    int age;

public:
    Person() = default;
    Person(std::string name, int age) : name{name}, age{age} {}

    bool operator<(const Person &rhs) const
    {
        return this->age < rhs.age;
    }
    bool operator==(const Person &rhs) const
    {
        return (this->name == rhs.name && this->age == rhs.age);
    }
};

void findTest()
{
    std::cout << "\n=============================" << std::endl;

    std::vector<int> vec{1, 2, 3, 4, 5, 7, 78, 4, 69, 9, 12, 54, 235, 67};

    auto loc = std::find(std::begin(vec), std::end(vec), 12);

    if (loc != std::end(vec))
    {
        std::cout << "Found the number: " << *loc << std::endl;
    }
    else
    {
        std::cout << "Couldn't find the number" << std::endl;
    }

    std::list<Person> players{
        {"moe", 22},
        {"oogabooga", 68},
        {"spotlight", 70}};

    auto loc1 = std::find(std::begin(players), std::end(players), Person{"moe", 22});

    if (loc1 != players.end())
    {
        std::cout << "moe was found" << std::endl;
    }
    else
    {
        std::cout << "couldn't find moe" << std::endl;
    }
}

void countTest()
{
    std::cout << "\n===========================" << std::endl;
    std::vector<int> nums{1, 24, 4, 5, 2, 1, 1, 3, 4, 2, 1, 6, 0, 7};

    int num = std::count(nums.begin(), nums.end(), 1);
    std::cout << num << " occurances found" << std::endl;
}

void countifTest()
{
    std::cout << "\n===========================" << std::endl;
    std::vector<int> nums{1, 24, 4, 5, 2, 1, 1, 3, 4, 2, 1, 6, 0, 78, 12};

    std::cout << "Vector size is "<< nums.size() << std::endl;
    int num = std::count_if(nums.begin(), nums.end(), [](int x) { return x % 2 == 0; });
    std::cout << num << " even numbers found" << std::endl;


    num = std::count_if(nums.begin(), nums.end(), [](int x) { return x%2 != 0; });
    std::cout << num << " odd numbers found" << std::endl;
}
void stringTransformTest()
{
    std::cout << "\n===========================" << std::endl;
    std::string test {"This is a test for corona and rejection virus"};

    std::cout << test << std::endl;

    std::transform(test.begin(), test.end(),test.begin(), ::toupper);
    std::cout << test << std::endl;
}

int main(int argc, char const *argv[])
{
    findTest();
    countTest();
    countifTest();
    stringTransformTest();
    return 0;
}
