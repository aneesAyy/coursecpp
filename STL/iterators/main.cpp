#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <list>

void display(const std::vector<int> &vec)
{
    std::cout << "[ ";
    for (auto &&i : vec)
    {
        std::cout << i << " ";
    }
    std::cout << " ]" << std::endl;
}
void test1()
{
    std::vector<int> nums1{1, 2, 3, 4, 5};
    display(nums1);

    // reverse display
    auto it2 = nums1.crbegin();
    while (it2 != nums1.crend())
    {
        std::cout << *it2 << std::endl;
        it2++;
    }
}

// Iterating over a map
void test2()
{
    std::map<std::string, std::string> map1{
        {"frank", "c++"},
        {"Booga", "java"},
        {"Noine", "assembleSHt"}};

    auto it3 = map1.begin();

    while (it3 != map1.end())
    {
        std::cout << it3->first << ": " << it3->second << std::endl;
        it3++;
    }
}

void test3()
{
    // iterate over a subset of the container
    std::vector<int> vec{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    auto start = vec.begin() + 3;
    auto finish = vec.end() - 2;

    while (start != finish)
    {
        std::cout << *start << std::endl;
        start++;
    }
}

int main(int argc, char const *argv[])
{
    // test1();
    // test2();
    test3();
    return 0;
}
