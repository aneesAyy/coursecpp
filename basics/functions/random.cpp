#include <iostream> 
#include <cstdlib> // for rand()
#include <ctime> // for time()

using namespace std;

int main(int argc, char const *argv[])
{
    int randomNumber {};
    size_t count {5};
    int min {1};
    int max {50};
    
    cout << "The RAND_MAX on this system is " << RAND_MAX << endl;

    srand(time(nullptr)); // To create a random number each time program is run
    
    for (size_t i = 0; i < count; i++)
    {
        randomNumber = rand() % max + min;
        cout << randomNumber << endl;
    }
    

    return 0;
}
