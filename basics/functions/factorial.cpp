#include <iostream>
using namespace std;

unsigned long long factorial (long long n)  { // long long because fatorial can be very large
    
    if (n == 0) // This is *important to start freeing up the stack
        return 1;   

    return n * factorial(n-1); // recursive case 
}

int main(int argc, char const *argv[])
{
    int num {};
    cout << "\nInput a number to find its factorial!" << endl;
    cin >> num;
    cout << factorial(num) << endl;
    return 0;
}
