#include <iostream>
using namespace std;

int globe {2500}; // This is a global variable

void staticExample();

int main(int argc, char const *argv[])
{
    int num {1000};
        {   // Creates a new local block
            int num {12};   // Local variable
            cout << "\nnum inside local block is " << num << endl;
        }

    cout << "\nnum inside main is: " << num <<  endl;
    
    staticExample();
    staticExample();
    staticExample();

    return 0;
}

void staticExample() {
    static int num {500};   // Using static keyword num does not get reinitialized when function is called again
    num += 200;
    cout << "\nThe number is now: " << num << endl;
}
