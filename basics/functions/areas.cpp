#include <iostream>

using namespace std;

// Function prototypes
void areaCircle();
void volume();

int main(int argc, char const *argv[])
{
    volume();
    areaCircle();
    
    return 0;
}

double calcArea (double radius) {
    const double pi {3.14};
    return pi * radius * radius;
}

void areaCircle () {
    cout << "Input radius of circle :" << endl;
    double radius {};
    cin >> radius;

    cout << "\nThe area of the circle is : " << calcArea(radius) << endl;
}

void volume () {
    cout << "Enter radius and height seperated by a space : " << endl;
    double radius {};
    double height {};

    cin >> radius;
    cin >> height;

    cout << "\nThe volume is : ";
    cout << (calcArea(radius) * height) << endl;
}
