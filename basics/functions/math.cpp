#include <iostream>
#include <cmath>
#include <iomanip>
// Using math functions from the standard c++ library

using namespace std;

int main(int argc, char const *argv[])
{
    cout << "Input a number (Double)" << endl;

    double num {};
    cin >> num;

// Using the functions 

    cout << "The square root is : " << sqrt(num) << endl;
    cout << "The cube root is : " << cbrt(num) << endl;
    cout << "The cosine is : " << cos(num) << endl;
    cout << "The ceil is : " << ceil(num) << endl;
    cout << "The floor is : " << floor(num) << endl;
    cout << "The round is : " << round(num) << endl;

    cout << "\nEnter a power to raise your number to :";
    int power {};
    cin >> power; 
    cout << "\nThe number raised to the power " << power << " is " << pow(num, power);

    return 0;
}
