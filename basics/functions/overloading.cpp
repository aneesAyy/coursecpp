#include <iostream>
#include <string>

using namespace std;

void print (int a) {
    cout << "The int value is : "<< a << endl;
}

void print (double a) {
    cout <<"The double value is : " << a << endl;
}

void print (string a) {
    cout <<"The string value is : " << a << endl;
}

int main(int argc, char const *argv[])
{
    print("This is something");
    return 0;
}
