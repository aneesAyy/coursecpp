#include <iostream>
#include <vector>
#include <string>

using namespace std;

void addValue(vector <int> &v); // Using "&" to edit a vector or a variable inside a functions
void printVector(const vector <int> v);

int main(int argc, char const *argv[])
{
    vector <int> nums {12, 43, 69, 23, 2, 122};
    printVector(nums);
    addValue(nums);
    printVector(nums);
    return 0;
}

void addValue(vector <int> &v) {
    cout << "\nEnter a number to add: " << endl;
    int number {};
    cin >> number;
    v.push_back(number);
}

void printVector(const vector <int> v) {
    cout << "The numbers are :" << endl;
    cout << "[ ";
    for (auto &&i : v) 
    {
        cout << i << " ";
    }   
    cout << "]" << endl;
}
