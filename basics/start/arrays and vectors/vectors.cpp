#include <iostream>
#include <vector>
using namespace std;

int main(int argc, char const *argv[])
{
    
    vector <int> studentRoll {1,3,4,5,7};

    cout << "\nInput Values for the vector : " << endl;

    for (int i = 0; i < studentRoll.size(); i++)
    {
        cin >> studentRoll.at(i);
    }

    cout << "\nThe values stored in the vector are : " << endl;

    for (int i = 0; i < studentRoll.size(); i++)
    {
        cout << studentRoll.at(i) << endl;
    }
    
    cout << "The size of the vector is : " << studentRoll.size() << endl; 
    return 0;
}
