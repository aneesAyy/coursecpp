#include <iostream>
#include <vector>
using namespace std;

int main(int argc, char const *argv[])
{
    vector <int> viewers;
    vector <int> movies;
    
    // Declaring a 2D vector that inputs all values of both vectors using a loop
    vector <vector <int>> reviews;

    reviews.push_back(viewers);    
    reviews.push_back(movies);

    const int noViewers {3};
    const int noMovies {5};

    viewers.push_back(1);
    viewers.push_back(2);

    movies.push_back(9);
    movies.push_back(6);
    movies.push_back(10);


    cout << "\nThe values in vectors are : " << endl;

    for (int i = 0; i < viewers.size(); i++)
    {
        cout << "\nFor Viewer Number:  " << viewers.at(i) << endl;
        for (int j = 0; j < movies.size(); j++)
        {
            cout << movies.at(j) << endl;    
        }
    }
    return 0;
}
