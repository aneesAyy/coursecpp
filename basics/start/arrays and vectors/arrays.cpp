#include <iostream> 
using namespace std;

int main() {
    const int noOfStudents {5};

    int scores[noOfStudents] {0};
    
    for (int i = 0; i < noOfStudents; i++)
    {
        cin >> scores[i];
    }
    
    cout << "The Scores are as follows " << endl << "==========================" << endl;
    for (int i = 0; i < noOfStudents; i++)
    {
        cout << scores[i] << "\t";
    }
    

    return 0; 
}