#include <iostream>
#include <vector>

using namespace std;

int main(int argc, char const *argv[])
{
    int total {};
    vector <int> userNumbers{};

    cout << "How many numbers are there" << endl;
    int userCount {};
    cin >> userCount;

    // We can take any number of inputs using a vector as follows
    cout << "Input numbers" << endl; 
    for (int i = 1; i <= userCount; i++)
    {
        int userInput {};
        cin >> userInput;
        userNumbers.push_back(userInput);
    }
    
    int sum {};
    // Now to add all the numbers in the vector and divide by total number
    for (int i = 0; i < userNumbers.size(); i++)
    {
        sum += userNumbers.at(i);
    }

    cout << "The sum is : " << sum << endl;
    int average = static_cast<double> (sum) / userCount;
    cout << "The average of the numbers is : " << endl << average << endl;
    return 0;
}
