#include <iostream>
using namespace std;

int main () {
    const int viewers {2};
    const int movies {2};

    int reviews [viewers] [movies] {};
    
    for (int i = 0; i < viewers; i++)
    {   
        cout << "Input Viewer number : "  << endl; 
        cin >> reviews[i] [0];
        cout << "Input Review Please!" << endl;

        int movieNumber {1};
        
        for (int i = 0; i < movies; i++)
        {
            
            cout << "For movie " << movieNumber << endl;
            cin >> reviews [0] [i]; 
            movieNumber += 1;
        }
    }

    cout << "Movie reviews are as follows..." << endl << "============================================" << endl;
    // cout << "For viewer 1 review is " << reviews[0] [0] << "\t"<< reviews [0] [1] << endl;
    for (int i = 0; i < viewers; i++) {
        
        cout << "For Viewer number : ";
        cout <<reviews[i] [0] ;
        cout << "Review for movies are..." << endl;
        int movieNumber {1};
        for (int i = 0; i < movies; i++)
        {
            cout << "For movie Number 1 : \t";
            cout << reviews[0] [i] << endl;
            movieNumber += 1;
        }  
    }    

    return 0;
}