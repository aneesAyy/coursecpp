#include <iostream>
#include <string>

// Makes a substitution cipher that encrypt an input message and then decrypts it
// Using c++ string functions 

using namespace std;

int main(int argc, char const *argv[])
{
    string alphabet {"ABCDEFGFIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz 1234567890"};
    string key {"VUEQOSTIRHMKNGPBDJYXZLCAWFxawsqzfcdvrehbgnytuijkmlop'(!@#*&!%)-"}; 

    cout << "Input a message to encrypt" << endl;
    string message {};
    getline(cin, message);
    
    cout << "\nEncrypting..." << endl;
    string encrypted {};
    for (auto && val : message)
    {   
        int position = alphabet.find(val);
        if (position != string::npos) {
            encrypted += key.at(position);
        } else {
            cout << val;
        }
    }
    cout << "The encrypted message is : " << encrypted;
    cout << "\nDecrypting message..." << endl;
    cout << "The decrypted message is :";
    string decrypted {};

    for (auto j : encrypted) {
        int position = key.find(j);
        decrypted.push_back(alphabet.at(position));       
    }

    cout << decrypted << endl;

    return 0;
}
