#include <iostream>
#include <cstring> // For C-style-string functions
#include <cctype> // For character functions
using namespace std;

int main(int argc, char const *argv[])
{
    char firstName [20] {};
    char lastName [20] {};
    char fullName[50] {};

    cout << "Input your first name" << endl; 
    cin >> firstName;
    
    cout << "Input your last name" << endl; 
    cin >> lastName;
    
    cout << "----------------------------------------------"
    cout << "Hello " << firstName << ". Your first name has " << strlen(firstName) << " characters" << endl;
    cout << "Your last name has " << strlen(lastName) << " characters" << endl;

// Some string functions

    strcpy(fullName, firstName); // Copy firstname to fullName
    strcat(fullName, " "); // Adds a whitespace
    strcat(fullName, lastName); // Copy lastName to full name 

    cout << "Your full name is " << fullName << endl;

// The get line function
    cout << "Enter Your full name" << endl;
    // cin >> fullName // Standard cin function only displays first word
    cin.getline(fullName, 50); // To make the whole line run (Till character limit 50)
    cout << "Your name is " << fullName; // Prints only first names because cout stops running at the whitespace
    
    return 0;
}
