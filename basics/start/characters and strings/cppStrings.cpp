#include <iostream>
#include <string>
using namespace std;

int main(int argc, char const *argv[])
{
    string s0 {"appla"};
    string s1 {s0, 1, 2}; // pp
    string s2 (5, 'N'); // NNNNNN
    string s3 {"baniga"};

// // Looping 
//     for (size_t i = 0; i < s0.length(); i++)
//     {
//         cout << s0.at(i) << endl;
//     }

// // Ranged for   
// for (auto &&i : s0)
// {
//     cout << i << endl;

// C++ String Functions
// Erase 

    // s0 = {"This no my niga"};
    // s0.erase(0,5); // no my niga
    // cout << s0 << endl;

// getline

    // cout << "Enter full name" << endl;
    // string fullName {};
    // getline(cin, fullName);
    // cout << "Hey there, " << endl;

// find

    string secret {"There is something fishy fishy"};

    string word {};
    cout << "Enter the word to find" << endl;
    cin >> word;

    size_t position = secret.find(word);

if (position != string::npos)
{
    cout << word << " found at position " << position << endl;
} else
{
    cout << word << " not found";
}



}
