#include <iostream>
using namespace std;
// Program convert USD to PKR


int main(int argc, char const *argv[])
{
    const double usdToPkr {1.57};

    cout << "Type in amount in USD to convert : " << endl;
  
    double usd;
    cin >> usd;

    double pkr = usd * usdToPkr;

    cout << "\nThe amount in PKR is : \t Rs." << pkr;


    cout << endl;
    return 0;
}
