#include <iostream>
using namespace std;

int main(int argc, char const *argv[])
{
    bool wearCoat {false};
    double temperature {0};
    double windSpeed {};

    const double tempForCoat {13};
    const int windSpeedForCoat {50};

    cout << "Enter the temperature in (C) :" << endl;
    cin >> temperature;

    cout << "Enter the wind speed in (mph) :" << endl;
    cin >> windSpeed;

    cout << boolalpha;

    cout << "Wear a coat : "  << (windSpeed > windSpeedForCoat || temperature < tempForCoat);

    return 0;
}
