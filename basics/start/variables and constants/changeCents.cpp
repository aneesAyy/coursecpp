#include <iostream>
using namespace std;
/********************************************************* 
 * Program that converts cents into change
 * Take cents input
 * 
**********************************************************/
int main(int argc, char const *argv[])
{
    cout << "Input an amount in cents: " << endl;
    int amount {};
    cin >> amount;

    int dollar {amount / 100};
    cout << "Dollars:" <<  dollar << endl;
    

    amount %= 100;
    int quarters {amount / 25};
    cout << "Quarters: " << quarters << endl;
  
    amount %= 25;
    int dimes {amount / 10};
    cout << "Dimes: " << dimes << endl;

    amount %= 10;
    int nickel {amount / 5};
    cout << "Nickels: " << nickel << endl;
    amount %= 5;

    int pennies {amount};
    cout << "Pennies: " << pennies << endl;

    return 0;
}
