#include <iostream>
#include <climits>
using namespace std;

int main(int argc, char const *argv[])
{
    // long long distance {7'000'000'000};
    // cout << distance << endl;
    // return 0;
    
    // double num1 {10e120};
    // cout << num1 << endl;

    /* Using "SIZE OF"  */
    //cout << sizeof(double);

    cout << "Char is : " << sizeof(char) << " bytes" << endl;
    cout << "Int is : " << sizeof(int) << " bytes" << endl;
    cout << "Long Int is : " << sizeof(long) << " bytes" << endl;
    cout << "Long long is : " << sizeof(long long) << " bytes" << endl;
    
}
