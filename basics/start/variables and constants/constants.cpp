#include <iostream>
using namespace std; 
/*****************
A program to calculate room cost in a hotel, for practice idk xd

Psuedocode:
Display Cost per room
Ask number of rooms
Calculate total cost
Show result
*****************/ 
int main(int argc, char const *argv[])
{
    cout << endl << "Welcome to my crib, niqa" << endl;
//  For Small Rooms 

    cout << "How many small rooms you want ?" << endl;
    double numSmallRooms {0};
    cin >> numSmallRooms;    

// For Large Rooms

    cout << "How many large rooms you want ?" << endl;
    double numLargeRooms {0};
    cin >> numLargeRooms;    

    cout << "==================================================" << endl;
    cout << "Number of small rooms : " << numSmallRooms << endl;
    cout << "Number of large rooms : " << numLargeRooms << endl;
    
    const double costPerSmallRoom {30};
    const double costPerLargeRoom {50};
    
    cout << "Cost per small room :" << costPerSmallRoom << endl;
    cout << "Cost per large room : " << costPerLargeRoom << endl << endl;

    const double taxRate {0.06};
    double totalTax {(costPerLargeRoom * numLargeRooms * taxRate) + (costPerSmallRoom * numSmallRooms * taxRate)};
    double costWithoutTax {costPerLargeRoom * numLargeRooms + costPerSmallRoom * numSmallRooms};

    cout << "The cost without tax is : " << costWithoutTax << endl;
    cout << "The total tax is : " << totalTax << endl;

    double totalCost {costWithoutTax + totalTax};

    cout << "Estimated Total cost is : " << totalCost << endl;
    return 0;
}