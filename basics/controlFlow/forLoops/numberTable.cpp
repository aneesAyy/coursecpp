#include <iostream>
#include <vector>
#include <iomanip>
using namespace std;

// Takes an input and returns table of that input


int main(int argc, char const *argv[])
{
    int num {};
    cout << "Input a number greater than 0" << endl;
    cin >> num;

    cout << "Here is the table for your number <3 \n===========================" << endl;
    if (num > 0) {

        for (int i = num, j = 1; j < 11; j++)
        {
            cout << i << " * " << j << " = " << (i*j) << endl;
        }
    } else {
        cout << "wtf, give me a valid number" << endl;
    }
    return 0;
}
