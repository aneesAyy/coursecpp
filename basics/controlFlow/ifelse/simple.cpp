#include <iostream>
using namespace std;

int main(int argc, char const *argv[])
{
    int num {};
    const int min {10};
    const int max {25};

    cout << "Enter a number between " << min << " and " << max << endl;
    cin >> num;

    if (num > min)
    {
        cout << num << " is greater than " << min << endl;
        int diff {num - min};
        cout << num << " is " << diff << " greater than " << min << endl;
    } else 
    {
        
    }
    
    return 0;
}
