#include <iostream>
using namespace std;
// A different but better logic for asking inputs over and over

int main(int argc, char const *argv[])
{
    bool done {false};
    int num {};
    cout << "Enter a number between 5 and 15" << endl;

    while (!done) {
        cin >> num;
        if (num <= 5 || num >= 15 ) {
            cout << "WTF? A number between 5 and 15" << endl;
        } else {
            cout << "Thank, nub!" << endl;
            done = true;
        }
    }
    
    return 0;
}
