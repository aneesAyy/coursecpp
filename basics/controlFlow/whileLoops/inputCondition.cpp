#include <iostream>
using namespace std;
// Keeps asking for input until condition is satisfied

int main(int argc, char const *argv[])
{
    int userNumber {};

    cout << "Enter a number less than 100" << endl;
    cin >> userNumber;

    while(userNumber >= 100) {
        cout << "Number must be less than 100 " << endl;
        cin >> userNumber;
    }

    cout << "Thanks" << endl;
    return 0;
}
 