#include <iostream>
#include <vector>
using namespace std;
// Displays a histogram againts inputs for a list

int main(int argc, char const *argv[])
{
    cout << "Input number of numOfItems!" << endl;
    int numOfItems; 
    cin >> numOfItems;

    vector <int> data {};

    for (int i = 1; i <= numOfItems; i++)
    {
        int item {};
        cout << "Enter item no. " << i << " : ";
        cin >> item;
        data.push_back(item);
    }


    for (auto && val : data) // Ranged based for loop that displays data in vector
    {
        for (int i = 0; i < val; i++) // For each value 'n' in the vector, displays an 'n' number of hashes
        {
            if (i % 5 == 0) // For every 5th value, displays a '|'
            {
                cout << '|';
            } else {
                cout << '-';
            }
            
        }
        cout << endl;
    }
    

    return 0;
}
