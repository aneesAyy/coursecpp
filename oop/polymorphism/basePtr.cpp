#include <iostream>
#include <vector>

using namespace std;

class Account {
    public:
        virtual void withdraw() {
            cout << "Account withdraw" << endl;
        }
    virtual ~Account() {
        cout << "Account Destructor" << endl;
    };
};

class Savings: public Account {
    public:
        virtual void withdraw() override {
            cout << "Savings withdraw" << endl;
        }
    virtual ~Savings() {
        cout << "Savings Destructor" << endl;
    };
};

class Trust: public Account {
    public:
        virtual void withdraw() override {
            cout << "Trust withdraw" << endl;
        }
    virtual ~Trust() {
        cout << "Trust Destructor" << endl;
    };
};

int main(int argc, char const *argv[])
{
    cout << "=== Pointers ===" << endl;
    Account *p1 = new Account();
    Account *p2 = new Savings();
    Account *p3 = new Trust();
    
    // Account *array [] = {p1, p2, p3};
    // for (auto i = 0; i < 3; i++)
    // {
    //     array[i] ->withdraw();
    // }

    vector <Account *> bank = {p1, p2, p3};
    for (auto &&i : bank)
    {
        i ->withdraw();
    }
    
    
    cout << "=== Clean up ===" << endl;
    delete p1;
    delete p2;
    delete p3;
    
    return 0;
}
