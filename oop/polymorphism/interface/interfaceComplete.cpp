#include <iostream> 

using namespace std;

class IPrintable {
    friend ostream &operator<< (ostream &os, const IPrintable &obj);
    public:
        virtual void print(ostream &os) const = 0;
};

ostream &operator<< (ostream &os, const IPrintable &obj) {
    obj.print(os);
    return os;
};

class Account: public IPrintable {
    public:
        virtual void withdraw(double amount) {
            cout << "Account withdraw" << endl;
        }
    virtual void print(ostream &os) const override {
        os << "Account display";
    }
    virtual ~Account() {};
};

class Checking: public Account {
    public:
        virtual void withdraw(double amount) {
            cout << "Checking withdraw" << endl;
        }

    virtual void print(ostream &os) const override {
        os << "Checking display";
    }
    virtual ~Checking() {};
};

class Saving: public Account {
    public:
        virtual void withdraw(double amount) {
            cout << "Saving withdraw" << endl;
        }
    virtual void print(ostream &os) const override {
        os << "Saving display";
    }
    virtual ~Saving() {};
};

class Dog: public IPrintable {
    public:
        virtual void print(ostream &os) const override {
            os << "Woof! Here to make ur day <3";
        }
};

void print(const IPrintable &obj) {
    cout << obj << endl;
}

int main(int argc, char const *argv[])
{

    Dog *doggo = new Dog();
    cout << *doggo << endl;
    print(*doggo);
    // Account a;
    // Checking c;
    // Saving s;

    // cout << a << endl;
    // cout << c << endl;
    // cout << s << endl;

    Account *s = new Saving();
    cout << *s << endl;

    Account *c = new Checking();
    cout << *c << endl;

    return 0;
}
