#include <iostream> 

using namespace std;

class Account {
        friend ostream &operator<< (ostream &os, Account &acc);
    public:
        virtual void withdraw(double amount) {
            cout << "Account withdraw" << endl;
        }
    virtual ~Account() {};
};

ostream &operator<< (ostream &os, Account &acc) {
    os << "Account Display";
    return os;
};

class Checking: public Account {
        friend ostream &operator<< (ostream &os, Checking &acc);
    public:
        virtual void withdraw(double amount) {
            cout << "Checking withdraw" << endl;
        }
    virtual ~Checking() {};
};

ostream &operator<< (ostream &os, Checking &acc) {
    os << "Checking Display";
    return os;
};
class Saving: public Account {
        friend ostream &operator<< (ostream &os, Saving &acc);
    public:
        virtual void withdraw(double amount) {
            cout << "Saving withdraw" << endl;
        }
    virtual ~Saving() {};
};

ostream &operator<< (ostream &os, Saving &acc) {
    os << "Saving Display";
    return os;
};

int main(int argc, char const *argv[])
{
    // Account a;
    // Checking c;
    // Saving s;

    // cout << a << endl;
    // cout << c << endl;
    // cout << s << endl;

    Account *ptr = new Saving();
    cout << *ptr << endl;

    return 0;
}
