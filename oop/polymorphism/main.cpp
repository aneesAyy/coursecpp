#include <iostream>

using namespace std;

class Base
{
    public: 
        void sayHello() {
            cout << "This is a base class object" << endl;
        }
};

class Derived: public Base
{
    public:
        void sayHello () {
            cout << "This is a derived class object" << endl;
        }
};

void greetings(Base &obj) {
    cout << "Greetings! ";
    obj.sayHello();
}

int main(int argc, char const *argv[])
{
    Base a;
    a.sayHello();

    Derived d;
    d.sayHello();

    greetings(a);
    greetings(d);

    Base *p = new Derived();
    p -> sayHello();

    return 0;
}
