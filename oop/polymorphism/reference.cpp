#include <iostream>

using namespace std;

class Account {
    public:
        virtual void withdraw(double amount) {
            cout << "Accout withdraw:: " << amount << endl; 
        }

    virtual ~Account() {};
};

class Savings: public Account {
    public:
        virtual void withdraw(double amount) {
            cout << "Savings withdraw:: " << amount << endl; 
        }
    virtual ~Savings() {};

};

void doWithdraw(Account &account, double amount) {
    account.withdraw(amount);
}

int main(int argc, char const *argv[])
{
    // Account a;
    // Account &ref = a;
    // ref.withdraw(100);

    // Savings s;
    // Account &ref2 = s;
    // ref2.withdraw(200);
    // return 0;

    Account a1;
    Savings a2;

    doWithdraw(a1, 1000);
    doWithdraw(a2, 15000);

}
