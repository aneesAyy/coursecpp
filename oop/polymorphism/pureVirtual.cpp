#include <iostream>
#include <vector>

using namespace std;

class Shape {
    public:
        virtual void draw() = 0;
        virtual void rotate() = 0;
    virtual ~Shape () {};
};

class OpenShape: public Shape {     // Abstract
    public:
        virtual ~OpenShape() {};
};

class ClosedShape: public Shape {   // Abstract
    public:
        virtual ~ClosedShape() {};
};

class Circle: public ClosedShape {    // Concrete
    public:
        virtual void draw() override {
            cout << "Circle Drawn" << endl;
        }

        virtual void rotate() override {
            cout << "Circle rotated" << endl;
        }
};

class Line: public OpenShape {    // Concrete
    public:
        virtual void draw() override {
            cout << "Line Drawn" << endl;
        }

        virtual void rotate() override {
            cout << "Line rotated" << endl;
        }
};

class Square: public ClosedShape {    // Concrete
    public:
        virtual void draw() override {
            cout << "Square Drawn" << endl;
        }

        virtual void rotate() override {
            cout << "Square rotated" << endl;
        }
};

void screenRefresh (const vector<Shape *> &shapes) {
    cout << "===Refereshing===" << endl;
    for (const auto p : shapes)
    {
        p->draw();
    }
    
}

int main(int argc, char const *argv[])
{
    Shape *s1 = new Circle();
    Shape *s2 = new Line();
    Shape *s3 = new Square();

    vector <Shape *> shapes = {s1, s2, s3};
    // for (auto &&p : shapes)
    // {
    //     p->draw();
    // }

    screenRefresh(shapes);  // Same op as above
    
    // Shape *ptr = new Shape(); // Compiler Error Because Shape is Abstract Class 
    delete s1;
    delete s2;
    delete s3;
    return 0;
}
