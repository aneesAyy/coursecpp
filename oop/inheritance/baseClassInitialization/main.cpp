#include <iostream>

using namespace std;

class Base {
private:
    int value;
public:
    Base() : value{0} {
        cout << "Base no-args constructor" << endl;
    }
    Base(int x) :value{x} {
        cout << "Int Base constructor" << endl;
    }
    ~Base () {cout << "Base destructor called" << endl;}
};

class Derived : public Base {
private:
    int doubledValue{0};
public:
    Derived() : Base(), doubledValue{0} {cout << "Derived constructor no args" << endl;}
    Derived(int x) : Base{x}, doubledValue{x*2} {cout << "Derived Int constructer called" << endl;}
    ~Derived() { cout << "\nDerived destructor" << endl;}
};

int main(int argc, char const *argv[])
{
    // Base b;
    Derived d{123};
    return 0;
}
