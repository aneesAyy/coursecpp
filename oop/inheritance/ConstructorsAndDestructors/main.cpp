#include <iostream>

using namespace std;

class Base {
private:
    int value;
public:
    Base() {cout << "Base no args constructor" << endl;}
    Base(int x) : value{x} {cout << "Base (int) overloaded constructor" << endl;}
    ~Base() {cout << "Base destructor";}
};

class Derived: public Base {
    // using Base::Base;               // Base constructor gets called from here
private:
    int doubledValue;
public:
    Derived() :doubledValue{0} {cout << "Derived no args constructor" << endl;};
    Derived(int x) :doubledValue{x*2} {cout << "Derived overloaded cons" << endl;}; // If this is commented out code would still work (cus of "using in line 15")
    ~Derived() {cout << "\nDerived destructor called" << endl;}
};

int main(int argc, char const *argv[])
{
    cout << endl;
    // Base b{12};
    Derived d{123};
    return 0;
}
