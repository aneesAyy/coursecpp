#pragma once
#include "Account.h"

class savingsAccount: public Account
{
private:
    /* data */
public:
    double intRate;

    savingsAccount();
    ~savingsAccount();

    void deposit(double amount);
    void withdraw(double amount);
};

