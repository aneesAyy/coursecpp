#include <iostream>
#include "savingsAccount.h"

savingsAccount::savingsAccount()
    : intRate{3}
{
}

void savingsAccount::deposit(double amount) {
    balance = amount;
    std::cout << "\nSavings account deposited with " << amount << std::endl;
    std::cout << "Added interest ammount is " << ((intRate*balance)/100 + balance) << std::endl;
}

void savingsAccount::withdraw(double amount) {
    std::cout << "Savings account withdrawn with " << amount << std::endl;
}

savingsAccount::~savingsAccount()
{
}
