#pragma once
#include <string>

class Account
{
private:
public:
    std::string name;
    double balance;

    void setName(std::string n) {name = n;};    // Setter
    std::string getName() {return name;};

    void deposit(double amount);
    void withdraw(double amount);

    Account();
    ~Account();
};
