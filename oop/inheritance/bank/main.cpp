#include <iostream>
#include "savingsAccount.h"
#include "Account.h"

using namespace std;

int main(int argc, char const *argv[])
{
    cout << "\n===========================================" << endl;
    Account acc {};
    acc.setName("Stupad");
    acc.deposit(200);
    acc.withdraw(100);

    // Account *pAcc {nullptr}; 
    // pAcc = new Account();
    // pAcc -> deposit(12345);
    // pAcc -> withdraw(6969);

    // cout << endl;

    savingsAccount savAcc {};
    savAcc.deposit(562);

    return 0;
}
