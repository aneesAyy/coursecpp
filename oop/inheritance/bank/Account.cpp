#include <iostream>
#include "Account.h"

Account::Account()
    : balance {0.0}, name {"anAccount"} 
{   
}

void Account::deposit(double amount) {
    std::cout << "Ammount depositted is by " << getName() << " is " << amount << std::endl; 
}

void Account::withdraw(double amount) {
    std::cout << "The ammount withdrawn is by " << getName() << " is " << amount << std::endl;
}

Account::~Account() {
}
