#include <iostream>

using namespace std;

class Base {
    int value;
public:
    Base() : value{0} {
        cout << "Base no argsConstructor called" << endl;
    }
    Base(int x) : value{x} {
        cout << "Base (int) constructor called" << endl;
    }
    Base (const Base &source) :value {source.value} {
        cout << "Base copy constructor" << endl;
    }

    Base &operator = (const Base &rhs) {
        cout << "Base operator" << endl;
        if (this == &rhs) {
            return *this;
        } else {
            value = rhs.value;
            return *this;
        }
    }
    ~Base() {
        cout << "Base destructor called"<< endl;
    }
};

class Derived : public Base {
private: 
    int doubleValue;
public:
    Derived() : Base{} { cout << "Derived no args cons. called" << endl;}
    Derived(int x) : Base{x} {cout << "Derived (int) constructor called" << endl;}
   
    ~Derived() {cout << "Derived Destructor called" << endl;}

    Derived(const Derived &source) : Base(source), doubleValue{source.doubleValue} {
        cout << "Derived Copy constructor" << endl;
    }

    Derived &operator= (const Derived &rhs) {
        cout << "Derived operator=" << endl;
        if (this == &rhs)
            return *this;
        Base::operator = (rhs);
        doubleValue = rhs.doubleValue;
        return *this;
    }
};

int main(int argc, char const *argv[])
{
    // Base b {100};   // Overloaded int constructor
    // Base b1{b};     // Copy constructor
    // b = b1;         // Copy assignment
    
    Derived d{200};     // Derived constructor
    Derived d1{d};      // Derived copy constructor
    d = d1;             // Operator (Copy assignment)

    return 0;
}
