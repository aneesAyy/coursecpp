#include <iostream>
#include "Account.h"
#include "savAccount.h"

using namespace std;

int main(int argc, char const *argv[])
{
    cout << "========Account class================" << endl;
    Account a1 {1000};
    cout << a1 << endl;

    a1.deposit(2000);
    cout << a1 << endl;

    a1.withdraw(40000);
    cout << a1 << endl;

    cout << "\n====================Savings Account class===================" << endl;
    savAccount s1{1000, 5};
    cout << s1 << endl;

    s1.withdraw(200);
    cout << s1 << endl;

    s1.withdraw(1000);
    cout << s1 << endl;

    return 0;
}
