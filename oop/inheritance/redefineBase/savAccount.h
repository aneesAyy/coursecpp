#pragma once
#include "Account.h"

class savAccount : public Account {
    friend std::ostream &operator << (std::ostream &os, const savAccount &account);
private:
    double intRate{};
public:
    savAccount();
    savAccount(double balance, double intRate);
    void deposit(double amount); 
};