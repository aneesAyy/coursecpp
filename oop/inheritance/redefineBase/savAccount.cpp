#include "savAccount.h"

savAccount::savAccount(double balance, double intRate)
    : Account(balance), intRate{intRate} {

}

savAccount::savAccount() 
    : savAccount (0.0, 0.0) {

    }

void savAccount::deposit(double amount) {
    amount = amount + (amount * intRate/100);
    Account::deposit(amount);
}

std::ostream &operator << (std::ostream &os, const savAccount &account) {
    os << "Savings account balance: " << account.balance << " Interest rate: " << account.intRate << std::endl; 
}
