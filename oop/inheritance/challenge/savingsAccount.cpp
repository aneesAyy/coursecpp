#include "savingsAccount.h"

savingsAccount::savingsAccount (std::string name, double balance, double intRate) 
    : Account {name, balance}, intRate{intRate} {}

// Deposit

bool savingsAccount::deposit(double amount) {
    amount += amount * (intRate/100);
    return Account::deposit(amount);
}

std::ostream &operator<<(std::ostream &os, const savingsAccount &account) {
    os << "[Savings Account: " << account.name << " : " << account.balance << ", " << account.intRate << "%";
    return os;
}
