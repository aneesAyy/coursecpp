#ifndef _SAVINGSACCOUNT_H_
#define _SAVINGSACCOUNT_H_
#include "Account.h"

class savingsAccount : public Account {
    friend std::ostream &operator<<(std::ostream &os, const savingsAccount &account);
private:
    static constexpr const char *defName = "Unnamed Savings Account";
    static constexpr double defBalance = 0.0;
    static constexpr double defIntRate = 0.0;

protected:
    double intRate;
    
public:
    savingsAccount(std::string name = defName, double balance = defBalance, double intRate = defIntRate);
    bool deposit(double amount);
    // inherits the Account withdraw methods
};

#endif // _SAVINGSACCOUNT_H_