#ifndef _ACCOUNTUTIL_H_
#define _ACCOUNTUTIL_H_

#include <vector>
#include "Account.h"
#include "savingsAccount.h"

// helper functions for accounts class

void display(const std::vector <Account> &accounts);
void deposit(std::vector <Account> &accounts, double amount);
void withdraw(std::vector <Account> &account, double amount);

// helper functions for savings accounts class

void display(const std::vector <savingsAccount> &accounts);
void deposit(std::vector <savingsAccount> &accounts, double amount);
void withdraw(std::vector <savingsAccount> &accounts, double amount);
#endif