#include <iostream>
#include "accountUtil.h"

// Displays account objects in a Vector

void display(const std::vector <Account> &accounts) {
    std::cout << "\n========== Accounts =======================" << std::endl;
    for (auto &&acc : accounts)
    {
        std::cout << acc << std::endl;
    }   
}

// Deposits supplied ammount to each account object in the vector

void deposit(std::vector <Account> &accounts, double amount) {
    std::cout << "=========== Depositing to Accounts===========" << std::endl;
    for (auto &&acc : accounts)
    {
        if (acc.deposit(amount))
        {
            std::cout << "Deposit " << amount << " to " << acc << std::endl;
        } else
        {
            std::cout << "Failed deposit of amount " << amount << " to " << acc << std::endl;
        }
    }
}

void withdraw(std::vector <Account> &accounts, double amount) {
    std::cout << "=========== Withdrawing from Accounts===========" << std::endl;
    for (auto &&acc : accounts)
    {
        if (acc.withdraw(amount))
        {
            std::cout << "Withdraw " << amount << " to " << acc << std::endl;
        } else
        {
            std::cout << "Failed Withdraw of amount " << amount << " to " << acc << std::endl;
        }
    }
}

// For savings account

void display(const std::vector <savingsAccount> &accounts) {
    std::cout << "\n========== Accounts =======================" << std::endl;
    for (auto &&acc : accounts)
    {
        std::cout << acc << std::endl;
    }   
}

// Deposit all for savingsAccount

void deposit(std::vector <savingsAccount> &accounts, double amount) {
    std::cout << "=========== Depositing to Savings Accounts===========" << std::endl;
    for (auto &&acc : accounts)
    {
        if (acc.deposit(amount))
        {
            std::cout << "Deposit " << amount << " to " << acc << std::endl;
        } else
        {
            std::cout << "Failed deposit of amount " << amount << " to " << acc << std::endl;
        }
    }
}

void withdraw(std::vector <savingsAccount> &accounts, double amount) {
    std::cout << "=========== Withdrawing from Savings Accounts===========" << std::endl;
    for (auto &&acc : accounts)
    {
        if (acc.withdraw(amount))
        {
            std::cout << "Withdraw " << amount << " to " << acc << std::endl;
        } else
        {
            std::cout << "Failed Withdraw of amount " << amount << " to " << acc << std::endl;
        }
    }
}