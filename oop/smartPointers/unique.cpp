#include <iostream>
#include <memory>

class Test {
    private:
        int data;
    public:
        Test(): data(0) {std::cout << "Test constructor (" << data << ")" << std::endl; };
        Test(int data): data(data) {std::cout << "Test constructor (" << data << ")" << std::endl; };
        int getData() {return data;};
        ~Test() {std::cout << "Test destructor (" << data << ")" << std::endl;};
};

int main(int argc, char const *argv[])
{
    /* Bsic working
    std::unique_ptr<int> p1 {new int {100}};
    std::cout << "The pointer p1 is " << *p1 << std::endl;

    // some unique pointer methods
    std::cout << p1.get() << std::endl; // returns address
    p1.reset();                         // p1 is now null

    if(p1)
        std::cout << *p1 << std::endl;  // Wont execute

    // A better way to work with unique pointers
    std::unique_ptr <int> p2 = std::make_unique<int> (100);
    std::cout << "The pointer p2 is " << *p2 << std::endl;

    // Automatically deleted
    */

    // Test *t1 = new Test(200);
    // std::cout << t1->getData() << std::endl;
    // delete t1;

    // // Some ways to declare unique pointers

    // std::unique_ptr<Test> t2 {new Test{500}};
    // std::cout << "Method 1: " << t2->getData() << std::endl;

    // std::unique_ptr <Test> t3 = std::make_unique<Test>(1000);
    // std::cout << "Method 2: " << t3->getData() << std::endl;

    auto t4 = std::make_unique<Test> (6969);
    // std::cout << "Method 3: " << t4->getData() << std::endl;

    std::unique_ptr <Test> t5;
    // t5 = t4;             // Compiler error
    t5 = std::move(t4);     // t4 is now a nullPtr

    std::cout << t5->getData() << std::endl;   
    // std::cout << t4->getData() << std::endl;    // Gives Error
    if(!t4) 
        std::cout << "T4 is a nullptr" << std::endl;

    return 0;
}
