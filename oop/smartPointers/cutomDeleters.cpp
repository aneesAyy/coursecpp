#include <iostream>
#include <memory>

using namespace std;

class Test {
    private:
        int data;  
    public:
        Test(): data {0} {cout << "Test constructor (" << data << ")" << endl;};
        Test(int data): data {data} {cout << "Test constructor (" << data << ")" << endl;};
        int getData() const {return data;}
        ~Test() {cout << "Test destructor (" << data << ")" << endl;};
};

void myDeleter(Test *ptr) {
    cout << "This is a custom deletor" << endl;
    delete ptr;
}

int main(int argc, char const *argv[])
{   
    shared_ptr<Test> p1 (new Test (100), 
        [] (Test *ptr) {
            cout << "This is a custom deleter using lambda" << endl;
            delete ptr;
        }
    );
    cout << "Value of p1 is " << p1->getData() << endl;

    shared_ptr <Test> p2 (new Test (123), myDeleter);
    cout << "Value of p2 is " << p1->getData() << endl;
    return 0;
}
