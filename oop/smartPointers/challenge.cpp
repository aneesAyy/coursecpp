#include <iostream>
#include <memory>
#include <vector>

using namespace std;

class Test {
    private:
        int data;
    public:
        Test(): data{0} {cout << "\tTest constructor(" << data << ")" << endl;};
        Test(int data): data{data} {cout << "\tTest constructor(" << data << ")" << endl;};
        int getData() {return data;};
        ~Test() {cout << "\tTest destructor (" << data << ")" << endl;};
};

// Functions Prototypes
unique_ptr<vector<shared_ptr<Test>>> make();
void fill(vector<shared_ptr<Test>>, int num);

int main(int argc, char const *argv[])
{
    return 0;
}
