#include <iostream>
#include <memory>
#include <vector>

int main(int argc, char const *argv[])
{
    // std::shared_ptr<int> p1 = std::make_shared<int> (123);
    // std::cout << "Use count: " << p1.use_count() << std::endl;
    // std::cout << *p1 << std::endl;

    // std::shared_ptr<int> p2 {p1};
    // std::cout << "Use count: " << p1.use_count() << std::endl;

    // p1.reset();

    // std::cout << "Use count: " << p1.use_count() << std::endl;
    // std::cout << "Use count: " << p2.use_count() << std::endl;

    // Shared pointers in vectors 
    std::vector<std::shared_ptr<int>> vec;
    std::shared_ptr<int> ptr = std::make_shared<int>(120);
    vec.push_back(ptr);                                     // Copy allowed

    std::cout << ptr.use_count() << std::endl;              // 2

    // Automatically deleted
    return 0;
}
