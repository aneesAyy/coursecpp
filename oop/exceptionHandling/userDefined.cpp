#include <iostream>

using namespace std;

class DivideByZeroException {
};
class NegativeValueException {
};

double calcDensity(int mass, int volume) {
    if(volume == 0)
        throw DivideByZeroException();
    if(mass < 0 || volume < 0)
        throw NegativeValueException();
    return static_cast<double>(mass)/volume;
}

int main(int argc, char const *argv[])
{
    cout << "Enter mass and volume seperated by a space" << endl;
    int mass, volume; cin >> mass >> volume;

    double density;
    try {
        density = calcDensity(mass, volume);
        cout << "The density is " << density << endl;
    }
    catch (DivideByZeroException &ex) {
        cout << "ERROR - Cannot divide by zero" << endl;
    }
    catch (NegativeValueException &ex) {
        cout << "ERROR - Cannot have negative values" << endl;
    }

    cout << "======= End program =======" << endl;
    return 0;
}
