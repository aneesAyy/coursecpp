#include <iostream>

using namespace std;

void funcA() {
    cout << "Starting funcA" << endl;
    throw 0;
    cout << "Ending funcA" << endl;
}
void funcB() {
    cout << "Starting funcB" << endl;
    try {
        funcA();
    }
    catch (int &ex) {
        cout << "=== Error in funcB ===" << endl;
    }
    cout << "Ending funcB" << endl;
}
void funcC() {
    cout << "Starting funcC" << endl;
    funcB();
    cout << "Ending funcC" << endl;
}

int main(int argc, char const *argv[])
{
    cout << "Starting main" << endl;
    try {
        funcC();
    }
    catch (int &ex) {
        cout << "=== Error in main ===" << endl;
    }
    cout << "Ending main" << endl;
    return 0;
}
