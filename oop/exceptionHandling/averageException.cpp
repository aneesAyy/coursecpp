#include <iostream>

using namespace std;

double calcAverage(int sum, int total) {
    if (total == 0) {
        throw 0;
    } else if (sum < 0 || total < 0) {
        throw string{"Negative value error"};
    } else {
        return static_cast<double>(sum)/total;
    }
}

int main(int argc, char const *argv[])
{
    double average {};
    cout << "Enter sum and total seperated by a space" << endl;
    int sum, total; cin >> sum >> total;

    try { 
        average = calcAverage(sum, total);
        cout << "Result: " << average << endl;
    }

    catch (int &ex) {
        cout << "You cannot divide by zero" << endl;
    }

    catch (string &ex) {
        cout << ex << endl;
    }

    catch (...) {   // Catches all exceptions
        cout << "Unknown exception" << endl;
    }
    cout << "Bye" << endl;
    return 0;
}
