#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <limits>

using namespace std;

int main(int argc, char const *argv[])
{
    /*
    cout << "Input stream ----------------------" << endl;
    // From stream to variables
    string name{};
    int num{};
    double value{};

    string info{"Ooga 100 124.57"};

    istringstream iss{info};
    iss >> name >> num >> value;

    cout << left << setw(10) << name
         << setw(10) << num
         << setw(10) << value << endl;

    cout << "\nOutput stream ----------------------" << endl;
    // From variables to strings
    name = "Booga";
    num = 120;
    value = 90.53;

    ostringstream oss{};
    oss << setw(10) << left << name
        << setw(10) << num
        << setw(10) << value << endl;

    info = oss.str();
    cout << info;
    */

    int value{};
    string entry{};

    bool done = false;
    do
    {
        cout << "Enter an integer: ";
        cin >> entry;
        istringstream validate{entry};

        if (validate >> value)
            done = true;
        else
            cout << "Sorry, thats not an integer" << endl;
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
    } while (done == false);

    cout << "You entered " << value << endl;

    return 0;
}
