#include <iostream>
#include <fstream>

using namespace std;

int main(int argc, char const *argv[])
{
    ofstream outFile;

    outFile.open("myfile", ios::app);

    if (!outFile.is_open())
    {
        cerr << "ERROR - Unable to open file" << endl;
        return 1;
    }
    cout << "Enter something to write to the file" << endl;
    string input{};

    while (input != "-1")
    {
        getline(cin, input);
        outFile << input << endl;
    }

    outFile.close();
    return 0;
}
