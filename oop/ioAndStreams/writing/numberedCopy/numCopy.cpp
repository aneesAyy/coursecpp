#include <iostream>
#include <fstream>
#include <iomanip>
using namespace std;

int main(int argc, char const *argv[])
{
    ifstream inFile;
    inFile.open("source");
    if (!inFile.is_open())
    {
        cerr << "ERROR - Cant open file" << endl;
    }

    ofstream outFile;
    outFile.open("out");
    if (!outFile.is_open())
    {
        cerr << "ERROR - Cant create file" << endl;
    }

    string line{};
    int num{1};
    while (getline(inFile, line))
    {
        if (line != "")
        {
            outFile << left << setw(8) << num;
            num++;
        }
        outFile << line << endl;
    }
    cout << "----- COPY COMPLETE -----" << endl;
    return 0;
}
