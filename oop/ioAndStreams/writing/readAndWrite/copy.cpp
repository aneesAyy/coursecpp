#include <iostream>
#include <fstream>

using namespace std;

int main(int argc, char const *argv[])
{
    ifstream inFile;
    inFile.open("input");
    ofstream outFile;
    outFile.open("result");

    if (!inFile.is_open())
    {
        cerr << "Error Opening file" << endl;
        return 1;
    }
    if (!outFile.is_open())
    {
        cerr << "Error Creating file" << endl;
        return 1;
    }

    // string line{};
    // while (getline(inFile, line))
    // {
    //     outFile << line << endl;
    // }

    // Same using get/put character
    char c{};
    while (inFile.get(c))
    {
        outFile.put(c) << ".";
    }

    cout << "----- File copied -----" << endl;

    inFile.close();
    outFile.close();

    return 0;
}
