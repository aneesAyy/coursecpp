#include <iostream>
#include <fstream>
#include <iomanip>

using namespace std;

int main(int argc, char const *argv[])
{
    ifstream inFile{};
    inFile.open("poem.txt");

    if (!inFile.is_open())
    {
        cerr << "ERROR - Unable to open file" << endl;
        return 1;
    }

    // // Prints using getline 
    // string line{};
    // cout << "\nTHE FAIRY SONG " << endl;
    // while (getline(inFile, line))
    // {
    //     cout << line << endl;
    // }

    // Prints using unformatted get
    char c {};
    while (inFile.get(c))
    {
        cout << " " << c;
    }
    inFile.close();
    return 0;
}
