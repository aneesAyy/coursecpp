#include <iostream>
#include <fstream>
#include <iomanip>

using namespace std;

int main(int argc, char const *argv[])
{
    ifstream inFile;
    inFile.open("responses.txt");

    if(!inFile.is_open()) {
        cerr << "ERROR - Cant open file" << endl;
    }

    string key {};
    inFile >> key;

    string name {}, answer {};
    double average {};
    int total ,count {};

    // Menu
    cout << left << setw(20) << "NAMES" << "SCORE" << endl;
    cout << setfill('-') << setw(25) << '-' << setfill(' ')<< endl; // Just for a line

    // View names and scores
    while (inFile >> name >> answer)
    {
        int score {};
        for (size_t i = 0; i < 5; i++)
        {
            if(answer.at(i) == key.at(i))
                score++;
        }  
        total += score;
        ++count;
        cout << left << setw(20) << name << score << endl;   
    }

    // Footer
    cout << setfill('-') << setw(25) << '-' << setfill(' ')<< endl; // All this for a line

    average = static_cast<double>(total) / count;
    cout << setw(20) << "[Average]" << average << endl;

    inFile.close();
    return 0;
}
