#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

int main(int argc, char const *argv[])
{
    ifstream inFile;
    inFile.open("data.text");
    string name{};
    int num{};
    double total{};

    if (!inFile.is_open())
    { // Checks if file was properly opened
        cerr << "**** File open error ****" << endl;
        return 1; // Closes the program
    }

    while (inFile >> name >> num >> total)
    {
        // inFile >> name >> num >> total;
        cout << setw(10) << left << name
             << setw(10) << num
             << setw(10) << total << endl;
    }
    inFile.close();
    return 0;
}
