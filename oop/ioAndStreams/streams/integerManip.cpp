#include <iostream>
#include<iomanip>
using namespace std;

int main(int argc, char const *argv[])
{
    int num{97865};

    cout << showbase;       // Prefix for hexa, octal, decimal
    cout << uppercase;      // Uppercase prefix
    cout << showpos;        // Positive sign

    // cout << resetiosflags(ios::showbase);   // Reset showbase
    cout << dec << num << endl;
    cout << oct << num << endl;
    cout << hex << num << endl;

    cout << noshowbase << nouppercase << noshowpos; // Resets all
    cout << "------------------------" << endl;
    cout << dec << num << endl;
    cout << oct << num << endl;
    cout << hex << num << endl;
    return 0;
}
