#include <iostream>
#include <iomanip>
using namespace std;

void ruler() {
    cout << "12345678901234567890123456789012345678901234567890" << endl;
}

int main(int argc, char const *argv[])
{
    double num {13.45};
    string hello {"Hello"};

    ruler();
    // cout << left; // Left justifies (Right justifies is default)
    // cout << fixed;
    cout<< setw(10) << num << setw(10) << hello << setw(10) << hello << endl;
    cout << setw(10) << setfill('_') << num << setw(10) << hello << setw(10) << hello <<  endl;
    return 0;
}
