#include <iostream>

int main(int argc, char const *argv[])
{
    std::cout << std::boolalpha;            // To show true and false instead of 1 and 0
    // std::cout.setf(std::ios::boolalpha);    // Same as above
    std::cout << (12 < 10) << std::endl;
    std::cout << (12 > 10) << std::endl;

    std::cout << std::noboolalpha;          // Back to 1 and 0
    std::cout << (12 == 10) << std::endl;
    std::cout << (12 == 12) << std::endl;

    return 0;
}
