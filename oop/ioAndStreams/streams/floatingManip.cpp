#include <iostream>
#include <iomanip>

using namespace std;

int main(int argc, char const *argv[])
{
    double num {1989879.12};
    cout << num << endl;

    cout << setprecision(3);
    cout << num << endl;

    cout << fixed;
    cout << num << endl;

    cout << scientific << showpos;
    cout << num << endl;

    cout << "------ Back to defaults ------" << endl;
    // Back to defaults 
    cout << resetiosflags(ios::fixed | ios::scientific);
    cout << setprecision(6);
    cout.unsetf(ios::showpos);
    cout << num << endl;
    return 0;
}
