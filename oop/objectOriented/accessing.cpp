#include <iostream>
#include <string>

using namespace std;

class Player {
private:
    int xp {120};

public:

    string name {"Player"};
    int health {100};

    void talk(string say) {cout << endl << name << " says: " << say << endl;};
    bool isDead();

    void xpUp() {cout << "XP is now: " << (xp + 1000) << endl; };
};

int main(int argc, char const *argv[])
{
    Player meh;
    meh.name = "Oooga";
    meh.health = 89;
    // meh.xp = 12; // compiler error because private

    cout << meh.name << endl << meh.health << endl;
    meh.xpUp();
    meh.talk("u're gay");

    // Using a pointer

    Player *enemy {nullptr};

    enemy = new Player();

    enemy -> name = "Spotlight Uh";
    (*enemy).talk("This game sucks, go kill yourself");

    delete enemy;

    return 0;
}
