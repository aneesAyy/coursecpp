#include <iostream>

using namespace std;

class Deep {
private:
    int *data;
public:

    // methods
    void setData(int d){*data = d;}
    int getData() {return *data;}

    // constructor
    Deep (int value) {
        data = new int;
        *data = value;
    };

    // deep copy
    Deep(const Deep &source) 
        :Deep(*source.data){
            cout << "\nCopy constructior - deep" << endl;
    }

};


void display(Deep s) {
    cout << s.getData() << endl;

}

int main(int argc, char const *argv[])
{
    Deep obj1 {123}; 
    display(obj1);
    return 0;
}

