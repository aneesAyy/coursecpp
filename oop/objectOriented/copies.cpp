#include <iostream>
using namespace std;

class Shallow {
private:
    int *data;

public:
    // methods
    void setData(int n) {*data = n;}
    int getData() {return *data;}
    int *getAddress() {return data;}

    void displayData () {
        cout << "\nThe data is "<< getData() << endl;
    }

    // constructor
    Shallow(int value) {
        data = new int;
        *data = value;
    };

    // shallow copy
    Shallow(const Shallow &ref)
        : data{ref.data} {
            cout << "Value copied - copy shallow af" << endl;
    };
    
    // destructor
    ~Shallow() {delete data; cout << "\nshts gone now" << endl;};
};

int main(int argc, char const *argv[])
{
    //{
    Shallow stuff {65};
    cout << "The data is " << stuff.getAddress() << endl;
    //}

    Shallow moreStuff {stuff};
    moreStuff.setData(65623);
    //display

    moreStuff.displayData();


    cout <<  moreStuff.getAddress() << endl; // both stuff and moreStuff at the same address, if stuff gets destroyed, moreStuff is fked or atleast thats what i understood after fkin my mind up for 25 minutes, imma go to deep copies now

    return 0;
}
