#include <iostream>
#include "Player.h"

using namespace std;

int Player::numPlayers {0};

Player::Player(string n, int h) 
    : name{n}, health{h} {
        ++numPlayers;
        cout << "Active players:" << numPlayers << endl;
    };

int Player::getNumPlayers() {
    return numPlayers;
}


void getActivePlayers() {
    cout << Player::getNumPlayers << endl;
}

int main(int argc, char const *argv[])
{
    //getActivePlayers();
    Player ooga("Frank", 12);
    Player booga("alcohal", 12);
    Player sad("not happy", 123);

    return 0;

}
