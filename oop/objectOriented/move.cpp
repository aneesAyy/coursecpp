#include <iostream>
#include <vector>

using namespace std;


class Move {
private:
    int *data;
public:
    int getDataValue() {return *data;}
    void setDataValue(int n) { *data = n;}

    Move(int d) {
        data = new int;
        *data = d;
        cout << "\nContructor for " << *data << endl;
    };

    Move(Move &&source) 
        : data(source.data) {       // Move contructor
        source.data = nullptr;      // Wipes away source
        cout << "\nMoving " << *data << endl;
    }        

    ~Move() {
        if (data != nullptr) 
            cout << "Deleting data for: " << *data << endl;
        else
            cout << "Deleting data for nullpointer" << endl; 
    } 
};

int main(int argc, char const *argv[])
{
    vector <Move> vec;
    vec.push_back(Move(12));
    vec.push_back(Move(90));

    return 0;
}
