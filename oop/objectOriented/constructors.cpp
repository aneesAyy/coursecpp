#include <iostream>
using namespace std;

class Player{
private:
    string name;
    int health;
    int exp;

public:
    //methods
    void setName(string n) {
        name = n;
    }
    
    string getName() {
        return name;
    }

    int getHealth() {
        return health;
    }
    
    // constructors
    // using initializers list
    // adding delegating constructors

    Player(string n, int h, int x) // delegating constructer
        : name{n}, health{h}, exp{x} {
    }
    
    Player() // using delegating constructor, only need to reinitialize
        : Player{"none", 0, 0} {
    }

    Player(string n) 
        : Player{name = n, 0, 0} {
    }


    // destructors

    ~Player() {
        cout << "\nDestructor called for " << name << endl;
    }

};

int main(int argc, char const *argv[])
{
    Player ooga("Ligma", 100, 12);
   // ooga.setName("niqqor");
    cout << ooga.getName() << "'s health is " << ooga.getHealth() << endl;
   

   // using a pointer constructor
    Player *enemy = new Player ("chunga", 1200, 120);
    cout << enemy -> getName() << endl;
    delete enemy;
   
    return 0;

}

