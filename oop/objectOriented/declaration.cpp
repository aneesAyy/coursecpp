#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Player {
    // artibutes
    string name {"Player"};
    int health {100};
    int xp;

    // methods
    void talk(string);
    bool isDead();

};

int main(int argc, char const *argv[])
{
    Player niqqa;
    Player boii;

    Player peeps[] {niqqa, boii};

    vector <Player> players {niqqa};
    players.push_back(boii);

    cout << players.size();

    Player *enemy {nullptr};
    enemy = new Player;
    // code
    delete enemy;

    return 0;
}
