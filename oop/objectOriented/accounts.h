#pragma once
#include <string>

class Accounts
{
private:
    std::string name;
    double balance;
public:
    void setName(std::string n) { name = n;};
    std::string getName() { return name;};

    void setBalance(double b);
    double getBalance();

    bool deposit(double d);
    bool withdrawl(double w);
};

void Accounts::setBalance(double b)
{
    balance = b;
}

double Accounts::getBalance()
{
    return balance;
}

bool Accounts::deposit(double d) {
    balance += d;
    return true;
}

bool Accounts::withdrawl(double w) {
    if (balance - w >= 0) {
        balance -= w;
        return true;
    }
    else 
        return false;
}
