#include <iostream>
#pragma once

class Player
{
private:
    std::string name;
    int health;
    static int numPlayers;

public:

    Player(std::string n, int h); // constructor

    void setName(std::string n);
    std::string getName();

    static int getNumPlayers();
   
    Player(const Player &source)      // copy constructor
        : Player {source.name, source.health} {}

    ~Player();          //destructor

};

void Player::setName(std::string n)
{
    name = n;
}

std::string Player::getName() {
    return name;
}

Player::~Player()
{
}

