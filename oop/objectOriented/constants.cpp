#include <iostream>

using namespace std;

class Player {
private:
    string name;
    int health;
public:
    // methods
    void setName(string n) { name = n; cout << "Name is now " << name << endl;}
    string getName() const {return name;}

    // construtor
    Player(string n, int h) 
        : name{n}, health{h} {
        }
    Player() 
        : Player{"none", 0} {
        }
};

void displayPlayerName(const Player &s) {
    cout << "\nThe name is " << s.getName() << endl;
}

int main(int argc, char const *argv[])
{
    const Player villain ("Enemy", 100);
    Player user("Player", 200);

    displayPlayerName(villain);
    displayPlayerName(user);

    user.setName("Oooga booga");
    // villain.setName("ThisWouldNotWork"); // ERROR
    
    return 0;
}
