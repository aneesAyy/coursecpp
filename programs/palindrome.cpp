#include <iostream>
#include <cctype>
#include <deque>
#include <string>
#include <vector>
#include <iomanip>
#include <algorithm>

bool isPalindrome(std::string &str)
{
    std::deque<char> data;
    for (auto &&c : str)
    {
        if (std::isalpha(c))
        {
            data.push_back(std::toupper(c));
        }
    }
    char c1{};
    char c2{};
    while (data.size() > 1)
    {
        c1 = data.front();
        c2 = data.back();
        if (c1 != c2)
        {
            return false;
        }

        data.pop_front();
        data.pop_back();
    }

    return true;
}

int main(int argc, char const *argv[])
{
    std::vector<std::string> testStrings{"sadidas" ,"lalalala", "lOloLoL", "aba", "abba", "abbcbba", "nasa at me", "A santa at NASA", "c++"};

    std::cout << std::boolalpha;
    std::cout << std::setw(10) << std::left << "Result"
              << "String" << std::endl;

    for (auto &&s : testStrings)
    {
        std::cout << std::setw(10) << isPalindrome(s) << s << std::endl;
    }

    std::string input {};
    std::cout << "\nInput a string to check" << std::endl;
    std::getline(std::cin, input);
    
    std::cout << "Palindrome: " << isPalindrome(input) << std::endl;

    return 0;
}
