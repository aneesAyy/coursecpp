#include <iostream>
#include <iomanip>
#include <set>
#include <map>
#include <string>
#include <fstream>
#include <sstream>

std::string cleanString(const std::string &s);

void displayWords(std::map<std::string, int> words)
{
    std::cout << std::left << std::setw(6) << "\nWord"
              << std::setw(20) << std::right << "Count" << std::endl;

    std::cout << "==========================" << std::endl;

    for (auto &&pair : words)
    {
        std::cout << std::setw(20) << std::left << pair.first
                  << std::setw(20) << pair.second << std::endl;
    }
};

void displayWords(std::map<std::string, std::set<int>> words)
{
    std::cout << std::left << std::setw(6) << "\nWord"
              << std::setw(20) << std::right << "Occurences" << std::endl;

    std::cout << "===============================" << std::endl;

    for (auto &&pair : words)
    {
        std::cout << std::setw(20) << std::left << pair.first
                  << '[';
        for (auto &&i : pair.second)
        {
            std::cout << i << " ";
        }
        std::cout << ']' << std::endl;
    }
};

void part1()
{
    std::map<std::string, int> words;
    std::string line;
    std::string word;
    std::ifstream inFile("words");
    char c{};

    if (!inFile.is_open())
    {
        std::cerr << "ERROR - can't open file" << std::endl;
    }
    else
    {
        while (std::getline(inFile, line))
        {
            std::stringstream ss(line);
            while (ss >> word)
            {
                word = cleanString(word);
                words[word]++;
            }
        }
        inFile.close();
        displayWords(words);
    }
}

std::string cleanString(const std::string &s)
{
    std::string result;
    for (auto &&c : s)
    {
        if (ispunct(c))
        {
            continue;
        }
        else
        {
            result += c;
        }
    }
    return result;
}

void part2()
{
    std::map<std::string, std::set<int>> words;
    std::string line;
    std::string word;
    std::ifstream inFile("words");

    if (!inFile.is_open())
    {
        std::cerr << "ERROR - Cant open file" << std::endl; //! Error
    }
    else
    {
        int lineNumber = 0;
        while (std::getline(inFile, line))
        {
            lineNumber++;
            std::stringstream ss(line);
            while (ss >> word)
            {
                word = cleanString(word);
                words[word].insert(lineNumber);
            }
        }
        inFile.close();
        displayWords(words);
    }
}

int main(int argc, char const *argv[])
{
    part1();
    // part2();
    return 0;
}
