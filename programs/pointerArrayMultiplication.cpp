#include <iostream>
using namespace std;
// A program that takes two arrays, multiplies their values with each other and returns a product array
// Using pointers ofcourse

int main(int argc, char const *argv[])


{
    // Prototype functions

    void print(int *arrayPtr, size_t size);
    int *applyAll(int *array1, size_t size1, int *array2, size_t size2);

    int array1[] {2, 4, 5, 6, 7};
    int size1 {5};
    int *arrayPtr1 {array1};

    int array2[] {10, 20, 30};
    int size2 {3};
    int *arrayPtr2 {array2};
    
// Printing both arrays 
    cout << "\nThe arrays are " << endl;
    print(array1, size1);
    print(array2, size2);
    applyAll(arrayPtr1, size1, arrayPtr2, size2);
 
    int *result = applyAll(arrayPtr1, size1, arrayPtr2, size2);
    cout << "\nThe address of resulting array is: " << result << endl;
    
    cout << "The array is: " << endl;
    print(result, 15);
    
    delete []result; // Should always delete, to free up memory in free store
    return 0;
}

void print (int *arrayPtr, size_t size) {
    cout << "[ ";
   for (size_t i = 0; i < size; i++)
   {
       /* All three statements below do the same thing */

       // cout << arrayPtr[i] << endl;
       cout << *arrayPtr++ << " "; 
       // cout << *(arrayPtr + i) << " "; 
   }
    cout << "]" << endl;
    
}

int *applyAll(int *array1, size_t size1, int *array2, size_t size2) {

    int *result{};
    result = new int[size1 * size2]; // 15

    int position{};
    
    for (size_t i = 0; i < size2; i++)
    {
        for (size_t j = 0; j < size1; j++)
        {
            result[position] = array2[i] * array1[j];
            ++position;
        }
        
    }

    return result;
}

