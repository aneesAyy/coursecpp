#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
using namespace std;

void ruler() {
    cout << "1234567890123456789012345678901234567890123456789012345678901234567890" << endl;
}

struct City 
{
    string name;
    long population;
    double cost;
};

struct Country
{
    string name;
    vector<City> cities;
};

struct Tours
{
    string title;
    vector<Country> countries;
};


int main(int argc, char const *argv[])
{
    // DATA
    Tours tours 
    {"Tours ticket prices",
        {
            {
                "Alabama", 
                {
                    {"NoCousinShite", 12400000, 69.69},
                    {"Call", 123400, 1243.00},
                    {"Hombries", 19800000, 870.99}
                }
            },
            {
                "ChingChong", 
                {
                    {"AsianPeople", 90847000, 1230.99},
                    {"MoreAsians", 123400, 12523.42},
                    {"Indialol", 19800000, 900.99}
                }
            },
            {
                "Chile",
                {
                    {"CusImChillin", 1251200, 990.23},
                    {"Call", 530000, 5133.00},
                }
            },
            {
                "PlaysFootball",
                {
                    {"FootballCity", 297900, 90.23},
                    {"Dontcall", 698800, 124.00},
                }
            }
        }
    };

    // Unformatted Display
    ruler();
    cout  << setw(70) << setfill('-') << '-' << endl; cout << setfill(' ');
    cout << setw(45) << tours.title << endl;
    cout << left << setw(70) << setfill('-') << '-' << endl;
    cout << left << setw(20) << "Country" << setw(20) << "City " << setw(20) << "Popuplation" << setw(10) << right << "Cost" << endl;
    cout << left << setw(70) << '-' << endl;
    cout << setfill(' ');
    cout << fixed << setprecision(2);
    for (auto &&country : tours.countries)
    {
        // for (auto &&city : country.cities)
        // {
        //     cout<< setw(20) << city.name
        //         << setw(20) << city.population
        //         << setw(20) << city.cost
        //         << endl;
        // }
        
        for (size_t i = 0; i < country.cities.size(); i++)
        {
            cout << left << setw(20) << ( (i==0) ? country.name: "");
            cout<< setw(20) << country.cities.at(i).name
                << setw(11) << right << country.cities.at(i).population
                << setw(19) << right <<country.cities.at(i).cost
                << endl;
        }
        
        cout << endl;
    }
    return 0;
}
