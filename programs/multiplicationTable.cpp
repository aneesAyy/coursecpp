#include <iostream>
using namespace std;
// Gives multiplication table for numbers 1 to 10

main () {
    for (int i = 1; i < 11; i++)
    {
        for (int j = 1; j < 11; j++)
        {
            cout << i << " * " << j << " = " << i*j << endl;
        }
        cout << "................." << endl;
    }
    
    return 0;
}