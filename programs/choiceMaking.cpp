/*****************************
 * Performs operations on a list of integers according to user input
 * The operations are PRINT, ADD, Calculate average, Display smallest or largest and QUIT.
******************************/
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int main(int argc, char const *argv[])
{
    vector <string> menu {
        "P - Print Numbers",
        "A - Add a Number",
        "M - Display Average of all numbers",
        "S - Print Smallest Number",
        "L - Print Largest Number",
        "C - Clear list",
        "Q - Quit"
    };

    for (auto && i : menu)
    {
        cout << i << endl;
    }

    vector <int> list {10, 20, 35, 40};
    char select {};
    do
    {   
        cout << "\nEnter a choice:" << endl;
        cin >> select;

        switch (select)
        {
        case 'P':
        case 'p': // Displays the list using ranged for loop
            cout << "The list is : " ;
            cout << '[';
            for (auto &&i : list)
            {
                cout << i << ", "; 
            }
            cout << ']';
            break;

        case 'a': // Takes input and add to the vector using push_back function
        case 'A': {
            cout << "Input a number to add : ";
            int num {};
            cin >> num;
            list.push_back(num);
            cout << num <<  " was added!" << endl;                    
            break;
        }
        case 'q': // Quits the program as defined by the 'while' function at the end
        case 'Q':
            cout << "\nExiting Program" << endl;
            break;
            
        case 'm': // Displays the average
        case 'M':{
            double sum {0};
            for (auto &&i : list)   // Add the items in list
            {
                sum += i;
            }
            double average {sum / list.size()};  // Divides by number of items in list
            cout << "The average is : " << average << endl;
            break;
        }

        case 'c': // To empty list 
        case 'C':
            list.clear(); // Simple command, thats all 
            cout << "The list is now empty" << endl;
            break;

        case 's': // Gives smallest element in the list, included "algorithm" package for this command to work
        case 'S': {
            int min {*min_element(list.begin(), list.end())};
            cout << "The smallest number is : " << min << endl;
            break;
        }

        case 'l':   // Gives largest number, same as above
        case 'L': {
            int max {*max_element(list.begin(), list.end())};
            cout << "The largest number is : " << max << endl;
            break;
        }
        
        default:
            cout << "Stop being gae and select a valid choice!" << endl; // And u gae even if u give a valid choice haha
            break;
        }
    } while (select != 'q' && select != 'Q'); // Ends the program if choice is 'q'
    cout << "Goodbye, niga <3" << endl;
    return 0;
}


