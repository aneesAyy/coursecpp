#include <iostream>
#include "Movie.h"

// Implementation of Movie.h
// Implementation of the constructor
Movie::Movie(std::string name, std::string rating, int watched) 
    : name(name), rating(rating), watched(watched)  {
    };

// Implementation of the copy constructor
Movie::Movie(const Movie &source)  
    : Movie{ source.name, source.rating, source.watched } {};

void Movie::display() const {
std::cout << name << ", " << rating << ", " << watched << std::endl;
}

// Implementation of the destructor
Movie::~Movie() {
}
