/*************************************
 * Model a movie with attributes
 * Name, rating and times wattched
**************************************/
#pragma once
#include <string>

class Movie
{
private:
    std::string name;
    std::string rating;
    int watched;
public:
    // Constructor
    Movie(std::string name, std::string rating, int watched);

    // copy Constructor
    Movie(const Movie &source);
    // destructor
    ~Movie();

    //Getters and setters
    void setName(std::string name) { this->watched = watched; };
    std::string getName() const {return name;}

    void getRating(std::string rating) { this->rating = rating; };
    std::string getRating() const {return rating;}

    void setWatched(int watched) { this->watched = watched; };
    int getWatched() const {return watched;}

    // Increment watched by 1
    void incrementWactch() { ++watched;}

    void display() const;

};
