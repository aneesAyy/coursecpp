#include <iostream>
#include "Account.h"

Account::Account()
    : balance {0.0}, name {"An Account"} 
{   
}

void Account::deposit(double amount) {
    std::cout << "Ammount depositted is " << amount << std::endl; 
}

void Account::withdraw(double amount) {
    std::cout << "The ammount withdrawn is " << amount << std::endl;
}
