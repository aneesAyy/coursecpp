#include <iostream>
#include <vector>
#include <iomanip>
using namespace std;

int main(int argc, char const *argv[])
{   
// Simple for loop
    // cout << "Simple for loop " << endl;
    // for (int i = 0; i < 10; i++)
    // {
    //     if (i%2 == 0) cout << i << endl;
    // }
    
// Ranged Based For loop
    // cout << "\nRanged based for loop " << endl;
    // vector <int> marks{98, 78, 65, 69};
    // for (auto &&i : marks)
    // {
    //     cout << i << endl;
    // }

// Average Using Ranged based for Loop

    vector <double> temperature {};
    int userCount {};
    double average {};
    double sum {};

    cout << "\nInput number of temperatures to be averaged, between 0 and 10 " << endl;
    cin >> userCount;

    if (userCount > 0 && userCount < 10) {

        cout << "Input the temperatures " << endl;

        for (int i; i < userCount ;i++)
        {
            double userTemp {};
            cin >> userTemp;
            temperature.push_back(userTemp);
        }
    
        cout << fixed << setprecision(2);
    
        for (auto &&i : temperature)
        {
            sum += i;
        }
        cout << "The average temperature is : "<< (sum / userCount) << endl;
    
    } else {
        cout << "\nInput a valid number of temperatures!" << endl;
    }

    return 0;

}
