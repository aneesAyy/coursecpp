/*****************************
 * Performs operations on a list of integers according to user input
 * The operations are print, add, calculate average,clear list,  display smallest or largest number and QUIT.
******************************/

#include <iostream>
#include <vector>
#include <algorithm>
#include <cctype>

using namespace std;

// Prototype functions

void displayMenu();
void print(vector <int> v);
char input();
void add(vector <int> &list, int num);
void average(vector <int> list);
void clear(vector <int> &list);
void smallest(vector <int> list);
void largest(vector <int> list);
void find(vector <int> list, int num);

int main(int argc, char const *argv[])
{
    displayMenu();
    vector <int> list{2, 3, 12, 321 ,31};
    char choice {};

    do {
        cout << "\nEnter a choice!" << endl;
        cin >> choice; // Take input

        switch (toupper(choice)) // Convert choice character to uppercase
        {
        case 'P': // PRINT LIST
            print(list);
            break;
        
        case 'A': { // ADD A NUMBER TO LIST
            cout << "\nInput the number to add" << endl; 
            int toAdd {};
            cin >> toAdd;
            if (cin.fail())
            {
                cout << "Enter an integer" << endl;
            } else {
                add(list, toAdd);
            }
        }
        break;

        case 'F': {
            int target {};
            cout << "\nEnter a number to find: ";
            cin >> target;
            find(list, target);
        }
        break;

        case 'M':   // AVERAGE ALL NUMBERS IN LIST
            average(list);
            break;
        case 'C':   // Clear list
            clear(list);
            break;
        case 'S':   // Display smallest number in list
            smallest(list);
            break;
        case 'L':   // Display largest number
            largest(list);
            break;
        case 'Q':   // Quit Program
            cout << "\nExiting...\n" << "\nGoodbye, niqa <3" << endl;
            break;
        default:
            cout << "Give me a valid number please, like wtf bro?" << endl;
        }
    } while (choice != 'q' && choice != 'Q'); // q or Q choice ends the program
}


// Declaring functions 

void displayMenu() { 
    vector <string> menu {
    "P - Print Numbers",
    "A - Add a Number",
    "F -  Find a number",
    "M - Display Average of all numbers",
    "S - Print Smallest Number",
    "L - Print Largest Number",
    "C - Clear list",
    "Q - Quit"    
};
    for (auto &&i : menu) // Simple forrange to display menu
    {
        cout << i << endl;
    }
    
}

void print(vector <int> v) {
    cout << "[ ";
    for (auto &&i : v) // Simple forrange to display list
    {   
        cout << i << " ";
    }  
    cout << "]" << endl;
}

void add (vector <int> &list, int num) { // Pass by reference is used to be able to edit list
    list.push_back(num);
    cout << num << " was added!" << endl;
}

void average(vector <int> list) { // Displays average
    double sum {}; // Used double for decimal values

    if (list.size() == 0) {
        cout << "\nCannot calculate mean - The list is empty" << endl;
    } else {
        for (auto &&i : list)
        {
            sum += i;     // "i" which is int gets promoted to double
        }
        double result;
        result = sum / list.size(); // result is a double
        cout << "\nThe Average is: " << result << endl; 
    }
}

void clear(vector <int> &list) { // Add "&" to be able to edit the vector
    list.clear();
    cout << "\nThe list is now empty" << endl;
}

void smallest(vector <int> list) {
    int result {*min_element(list.begin(), list.end())}; // Using algorithem lib, extracts smallest value from a vector
    cout << "\nThe smallest number is: [ " << result << " ]" << endl;
}

void largest(vector <int> list) {
    int result {*max_element(list.begin(), list.end())}; // Using algorithem lib, extracts largest value from a vector
    cout << "\nThe largest number is: [ " << result << " ]" << endl;
}

void find(vector <int> list, int num) {
    bool found;
    
    for (auto &&i : list)
    {
        if (i == num) {
            found = true; 
        }
    }
    if (found == true)
    {  
        cout << num << " was found" << endl;
    } else
    {
        cout << num << " was not found" << endl;
    }
}