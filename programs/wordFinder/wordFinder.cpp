#include <iostream>
#include <fstream>

using namespace std;

int main(int argc, char const *argv[])
{
    ifstream inFile{};
    inFile.open("source.txt");

    if (!inFile.is_open())
    {
        cerr << "ERROR - Unable to open file" << endl;
        return 1;
    }

    string line{};
    cout << "Input a word to find to find" << endl;
    string input{};
    cin >> input;

    int allCounter, wordCounter{};
    while (inFile >> line)
    {
        allCounter++;

        // // Increase counter only on exact match
        // if (input == line)
        // {
        //     wordCounter++;
        // }

        // Increase counter if input also found in substring
        size_t found = line.find(input);
        if (found != string::npos)
        {
            wordCounter++;
            // cout << " " << line; // To display the words, where found is true
        }
    }
    cout << "\nSearched form " << allCounter << " words" << endl;
    cout << "The word \"" << input << "\" found " << wordCounter << " times" << endl;
    inFile.close();

    return 0;
}
