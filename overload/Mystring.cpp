#include <iostream>
#include <cstring> // To use c-style string functions 
#include "Mystring.h"

using namespace std;

Mystring::Mystring() : str(nullptr) { // no args constructor
    str = new char[1];
    *str = '\0';
}

//overLoaded Constructor
Mystring::Mystring(const char *s) : str(nullptr){
    if (s == nullptr)
    {
        str = new char[1];
        *str = '\0';
    } else {
        str = new char[strlen(s) + 1]; // +1 to add a null teminator
        strcpy(str, s);
    }
        
}

// Copy Constructor 
Mystring::Mystring(const Mystring &source) : str(nullptr) {
    str = new char[1];
    strcpy(str, source.str);
}

// destructor
Mystring::~Mystring() {
    delete [] str;
}

int Mystring::getLength() const {return strlen(str);};

void Mystring::display() const {
    cout << str << ": "<< getLength() << endl;
}
