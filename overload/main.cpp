#include <iostream>
#include "Mystring.h"

using namespace std;

int main(int argc, char const *argv[])
{
    Mystring empty;
    Mystring stuff("ooga");
    Mystring moreStuff(stuff);

    empty.display();
    stuff.display();

    cout << "This works" << endl;

    return 0;
}
