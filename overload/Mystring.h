
class Mystring {

private:
    char *str; // pointer to a char[] that hold a c-style string
public:
    Mystring();                         // constructor no args
    Mystring(const char *s);            // constructor overloaded
    Mystring(const Mystring &source);   // copy constructor
    ~Mystring();                        // destructor

    const char *getStr() const;
    void display() const;
    int getLength() const;              // getters
};